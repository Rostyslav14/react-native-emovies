import { StatusBar } from "expo-status-bar";
import { SafeAreaProvider } from "react-native-safe-area-context";
import { useCachedResources } from "./helpers/hooks/useCachedResources";
import Navigation from "./navigation";
import React from "react";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { styleVariables } from "./styles/variables";
import { registerRootComponent } from "expo";

function App() {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  } else {
    return (
      <Provider store={store}>
          <StatusBar
            style={"light"}
            backgroundColor={styleVariables.colors.greenDeepDark}
          />
          <SafeAreaProvider>
            <Navigation />
          </SafeAreaProvider>
      </Provider>
    );
  }
}
export default registerRootComponent(App);
