import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import { Image } from "react-native";

const Container = styled.View`
align-self:center;
  width: 100px;
  height: 100px;
  justify-content:center;
  align-items:center;
`;

interface IProps {}
export const AppLogo: FC<IProps> = (props) => {
  return (
    <Container>
      <Image style={{flex:1}} resizeMode="contain" source={require("../../../assets/icons/icon.webp")} />
    </Container>
  );
};
