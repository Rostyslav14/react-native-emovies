import { FC } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../styles/variables";
import React from "react";
import { ActivityIndicator, PressableProps } from "react-native";

const Button = styled.Pressable<{ isLoading: boolean }>`
  margin-top: 15px;
  background-color: ${(props) =>
    props.isLoading
      ? styleVariables.colors.greenWeak
      : styleVariables.colors.greenBright};
  text-align: center;
  justify-content: center;
  align-items: center;
  padding-top: 6px;
  padding-bottom: 6px;
  border-radius: 4px;
  flex-direction: row;
`;
const BtnText = styled.Text<{ isLoading: boolean }>`
  letter-spacing: 0.1px;
  font-weight: ${styleVariables.fontWeights.bold};
  color: ${(props) =>
    props.isLoading
      ? styleVariables.colors.greenBright
      : styleVariables.colors.greenWeak};
`;
interface IProps extends PressableProps {
  children: string;
  onPress: () => void;
  isLoading: boolean;
}
export const AuthBtn: FC<IProps> = (props) => {
  const { children, onPress, isLoading, ...rest } = props;
  return (
    <Button isLoading={isLoading} onPress={() => onPress()} {...rest}>
      {isLoading && (
        <ActivityIndicator
          size="small"
          color={styleVariables.colors.greenBright}
        />
      )}
      <BtnText isLoading={isLoading}>
        {isLoading ? " Loading..." : children}
      </BtnText>
    </Button>
  );
};
