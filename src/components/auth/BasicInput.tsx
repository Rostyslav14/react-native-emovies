import { FC } from "react";
import { styleVariables } from "../../styles/variables";
import { TextInput, TextInputProps } from "react-native-paper";
import { Text, View } from "react-native";
import React from "react";

interface IProps extends TextInputProps {
  leftIconName: string;
  field: any;
  form: any;
}

export const BasicInput: FC<IProps> = (props) => {
  const {
    field: { name, onBlur, onChange, value },
    form: { errors, touched, setFieldTouched },
    leftIconName,
    ...inputProps
  } = props;

  const hasError = errors[name] && touched[name];

  return (
    <View>
      <TextInput
        value={value}
        onChangeText={(text) => onChange(name)(text)}
        onBlur={() => {
          setFieldTouched(name);
          onBlur(name);
        }}
        textColor={styleVariables.colors.whiteBasic}
        outlineColor={hasError ? "red" : styleVariables.colors.whiteWeak}
        style={{
          backgroundColor: styleVariables.colors.greenMain,
          marginVertical: 12,
        }}
        activeOutlineColor={
          hasError ? "red" : styleVariables.colors.greenBright
        }
        mode="outlined"
        left={
          <TextInput.Icon
            icon={leftIconName}
            iconColor={styleVariables.colors.whiteWeak}
          />
        }
        {...inputProps}
      />
      {hasError && (
        <Text style={{ fontSize: 10, color: "red", alignSelf: "center" }}>
          {errors[name]}
        </Text>
      )}
    </View>
  );
};
