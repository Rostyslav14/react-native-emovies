import { FC } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../styles/variables";
import React from "react";
import { ENScreens } from "../../helpers/constants/routes";
import { Link } from "@react-navigation/native";
const Container = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  margin-top: 15px;
`;
const Text = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
`;
const LinkText = styled.Text`
  font-weight: ${styleVariables.fontWeights.medium};
  color: ${styleVariables.colors.greenBright};
`;
interface IProps {
  description: string;
  linkText: string;
  linkPage: keyof typeof ENScreens;
}
export const BottomLine: FC<IProps> = (props) => {
  const { description, linkText, linkPage } = props;
  return (
    <Container>
      <Text>{description}</Text>
      <Link
        style={{ marginLeft: 10 }}
        to={{
          screen: `${linkPage}`,
        }}
      >
        <LinkText>{linkText}</LinkText>
      </Link>
    </Container>
  );
};
