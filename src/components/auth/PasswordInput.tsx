import { FC, useState } from "react";
import { styleVariables } from "../../styles/variables";
import { TextInput, TextInputProps } from "react-native-paper";
import React from "react";
import { Text, View } from "react-native";

interface IProps extends TextInputProps {
  field: any;
  form: any;
}

export const PasswordInput: FC<IProps> = (props) => {
  const [secureTextEntry, setSecureTextEntry] = useState(true);
  const {
    field: { name, onBlur, onChange, value },
    form: { errors, touched, setFieldTouched },
    ...inputProps
  } = props;

  const hasError = errors[name] && touched[name];

  return (
    <>
      <TextInput
        value={value}
        onChangeText={(text) => onChange(name)(text)}
        onBlur={() => {
          setFieldTouched(name);
          onBlur(name);
        }}
        outlineColor={hasError ? "red" : styleVariables.colors.whiteWeak}
        textColor={styleVariables.colors.whiteBasic}
        style={{
          backgroundColor: styleVariables.colors.greenMain,
          marginVertical: 12,
        }}
        activeOutlineColor={
          hasError ? "red" : styleVariables.colors.greenBright
        }
        mode="outlined"
        secureTextEntry={secureTextEntry}
        left={
          <TextInput.Icon
            icon="lock"
            iconColor={styleVariables.colors.whiteWeak}
          />
        }
        right={
          <TextInput.Icon
            onPress={() => {
              setSecureTextEntry(!secureTextEntry);
              return false;
            }}
            icon={secureTextEntry ? "eye-off" : "eye"}
            iconColor={styleVariables.colors.whiteWeak}
          />
        }
        {...inputProps}
      />
      {hasError && (
        <Text style={{ fontSize: 12, color: "red", alignSelf: "center" }}>
          {errors[name]}
        </Text>
      )}
    </>
  );
};
