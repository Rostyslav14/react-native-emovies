import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import Feather from "react-native-vector-icons/Feather";
import { Pressable } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import { GoBackArrowBtn } from "../../shared/GoBackArrowBtn";

const Container = styled.View`
  padding: 15px 10px;
  flex-direction: row;
  background-color: ${styleVariables.colors.greenDeepDark};
  align-items: center;
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-size: 20px;
  font-weight: ${styleVariables.fontWeights.bold};
`;
interface IProps {
  title: string;
  navigation: any;
}
export const HeaderWithBackBtn: FC<IProps> = (props) => {
  const { title, navigation } = props;
  return (
    <SafeAreaView>
      <Container>
        <GoBackArrowBtn navigation={navigation} />
        <Title>{title}</Title>
      </Container>
    </SafeAreaView>
  );
};
