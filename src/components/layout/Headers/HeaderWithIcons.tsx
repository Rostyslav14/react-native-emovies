import { FC, ReactNode } from "react";
import styled from "styled-components/native";
import React from "react";
import { styleVariables } from "../../../styles/variables";
import { SafeAreaView } from "react-native-safe-area-context";
import { HeaderIoniIcon } from "../../shared/HeaderIoniIcon";
import { Pressable, View } from "react-native";
import Feather from "react-native-vector-icons/Feather";

const Container = styled.View`
  flex-direction: row;
  padding: 15px 10px;
  justify-content: space-between;
  align-items: center;
  background-color: ${styleVariables.colors.greenDeepDark};
`;
const LeftSideWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
const HeaderText = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-size: 20px;
  font-weight: ${styleVariables.fontWeights.bold};
  margin-right: 25px;
  max-width: 170px;
`;
const IconsWrapper = styled.View`
  flex-direction: row;
  margin-left: -10px;
  margin-right: -10px;
`;

interface IProps {
  title: string;
  navigation: any;
  children:ReactNode
}

export const HeaderWithIcons: FC<IProps> = (props) => {
  const {
    title,
    navigation,
    children
  } = props;
  return (
    <SafeAreaView>
      <Container>
        <LeftSideWrapper>
          <Pressable onPress={() => navigation.goBack()}>
            <Feather
              name="arrow-left"
              size={18}
              color={styleVariables.colors.greenBright}
              style={{ marginRight: 20 }}
            />
          </Pressable>
          <HeaderText numberOfLines={1} ellipsizeMode="tail">
            {title}
          </HeaderText>
        </LeftSideWrapper>
        <IconsWrapper>
          {children}
         
        </IconsWrapper>
      </Container>
    </SafeAreaView>
  );
};
