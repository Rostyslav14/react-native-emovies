import { FC, ReactNode } from "react";
import { SafeAreaView, SafeAreaProvider } from "react-native-safe-area-context";
import styled from "styled-components/native";
import { styleVariables } from "../../styles/variables";
import React from "react";
import { ErrorInformingModal } from "../../screens/ErrorInformingModal";

interface IProps {}

const Container = styled.View`
  background-color: ${styleVariables.colors.greenMain};
  flex: 1;
`;
interface IProps {
  children: ReactNode;
}
export const Layout: FC<IProps> = (props) => {
  //   const { top } = useSafeAreaInsets();
  return (
    <SafeAreaProvider>
      <SafeAreaView  style={{ flex: 1 }}>
        <Container>{props.children}</Container>
        <ErrorInformingModal/>
      </SafeAreaView>
    </SafeAreaProvider>
  );
};
