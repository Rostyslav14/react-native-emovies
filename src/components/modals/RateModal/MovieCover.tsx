import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import config from "../../../config";
import { LAYOUT } from "../../../helpers/constants/layout";
import { styleVariables } from "../../../styles/variables";
import { Image } from "react-native";

const Container = styled.View`
  width: ${LAYOUT.window.width}px;
  height: ${LAYOUT.window.height}px;
`;
const SmallImgContainer = styled.View`
  width: 120px;
  height: 180px;
  border-width: 1px;
  border-color: ${styleVariables.colors.whiteMain};
  align-self: center;
  margin-top: 20px;
`;
interface IProps {
  imageSrc: string;
}
export const MovieCover: FC<IProps> = (props) => {
  return (
    <SmallImgContainer>
      <Image
        style={{ flex: 1 }}
        source={{ uri: config.apiImgUrl + props.imageSrc }}
        resizeMode="cover"
      />
    </SmallImgContainer>
  );
};
