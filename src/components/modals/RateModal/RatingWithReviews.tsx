import { FC, ReactNode } from "react";
import { styleVariables } from "../../../styles/variables";
import { LAYOUT } from "../../../helpers/constants/layout";
import { AirbnbRating } from "react-native-ratings";
import React from "react";

interface IProps {
  children: ReactNode;
  onFinishRating: (number: number) => void;
  rating: number;
}
export const RatingWithReviews: FC<IProps> = (props) => {
  const { children, onFinishRating, rating } = props;
  return (
    <>
      <AirbnbRating
        size={16}
        reviewColor={styleVariables.colors.whiteBasic}
        count={10}
        reviews={["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]}
        selectedColor={styleVariables.colors.greenBright}
        starContainerStyle={{
          justifyContent: "space-between",
          width: LAYOUT.window.width - 100,
          marginBottom: 15,
        }}
        defaultRating={rating}
        onFinishRating={(number) => onFinishRating(number)}
      />
      {children}
    </>
  );
};
