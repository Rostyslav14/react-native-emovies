import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import { Image } from "react-native";
import config from "../../../config";
import { styleVariables } from "../../../styles/variables";

const Container = styled.View`
  padding-top: 10px;
  padding-left: 10px;
  padding-right: 10px;
  flex: 1;
  flex-direction: row;
  height: 200px;
`;
const ImgWrapper = styled.View`
  flex: 1;
  border-radius: 12px;
`;
const TextWrapper = styled.View`
  flex: 2;
  padding: 10px;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-size: ${styleVariables.fontSizes.giant};
  font-weight: ${styleVariables.fontWeights.biggest};
`;
const TopicLine = styled.View`
  flex-direction: column;
  margin-top: 10px;
`;
const TopicTitle = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-size: ${styleVariables.fontSizes.bold};
  font-weight: ${styleVariables.fontWeights.medium};
`;
const TopicText = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-size: ${styleVariables.fontSizes.biggest};
  font-weight: ${styleVariables.fontWeights.biggest};
`;

interface IProps {
  name: string;
  knownFor: string;
  birthPlace: string;
  birthDate: string;
  imgUrl: string;
}

export const ActorDetailsPreview: FC<IProps> = (props) => {
  const { name, knownFor, birthPlace, birthDate, imgUrl } = props;
  return (
    <Container>
      <ImgWrapper>
        <Image
          style={{ borderRadius: 8, flex: 1,borderWidth:1,borderColor:'white' }}
          resizeMode="cover"
          source={{ uri: config.apiImgUrl + imgUrl }}
        />
      </ImgWrapper>
      <TextWrapper>
        <Header>{name}</Header>
        <TopicLine>
          <TopicTitle>Known For</TopicTitle>
          <TopicText>{knownFor}</TopicText>
        </TopicLine>
        <TopicLine>
          <TopicTitle>Birth Place</TopicTitle>
          <TopicText>{birthPlace}</TopicText>
        </TopicLine>
        <TopicLine>
          <TopicTitle>Birth Date</TopicTitle>
          <TopicText>{birthDate}</TopicText>
        </TopicLine>
      </TextWrapper>
    </Container>
  );
};
