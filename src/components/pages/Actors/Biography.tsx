import styled from "styled-components/native";
import React, { FC } from "react";
import { styleVariables } from "../../../styles/variables";
const Container = styled.View`
  padding-left: 10px;
  padding-right: 10px;
  flex-direction: column;
  margin-top: 25px;
  padding-bottom: 10px;
`;
const Header = styled.Text`
  margin-bottom: 10px;
  color: ${styleVariables.colors.whiteMain};
  font-size: ${styleVariables.fontSizes.giant};
  font-weight: ${styleVariables.fontWeights.bold};
`;
const BioText = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-size: ${styleVariables.fontSizes.bold};
`;

interface IProps {
  biography: string;
}
export const Biography: FC<IProps> = (props) => {
  const { biography } = props;
  return (
    <Container>
      <Header>Biography</Header>
      <BioText>{biography}</BioText>
    </Container>
  );
};
