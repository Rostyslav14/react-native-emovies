import React, { FC, ReactNode } from "react";
import { styleVariables } from "../../../styles/variables";
import styled from "styled-components/native";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

const Container = styled.View`
padding-left:10px;
margin-top:15px;
margin-bottom:15px;
`
const AccountLine = styled.View`
  flex-direction: row;
  align-items: flex-end;
  margin-bottom:25px;
`;
const Header = styled.Text`
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: 26px;
  color: ${styleVariables.colors.whiteMain};
  margin-left:15px;
;
`;

interface IProps {
  children:ReactNode
  header:string
}

export const HeaderBlock: FC<IProps> = (props) => {
  const { children,header } = props
  return (
    <Container>
      <AccountLine>
        <MaterialCommunityIcons
          color={styleVariables.colors.greenBright}
          size={35}
          name="account-circle"
        />
        <Header>{header}</Header>
      </AccountLine>
      {children}
    </Container>
  );
};
