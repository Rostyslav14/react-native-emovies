import { FC, ReactNode } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import React from "react";
import { Ripple } from "../../shared/Ripple";
import { Dimensions, View } from "react-native";

const Container = styled.View`
  display: flex;
  flex-direction: row;

  padding: 10px;
`;
const IconWrapper = styled.View`
  margin-right: 10px;
  justify-content: center;
  align-items: center;
`;
const TextWrapper = styled.View`
  flex-direction: column;
  justify-content: center;
  margin-left: 20px;
`;
const HeaderText = styled.Text`
  color: ${styleVariables.colors.greenBright};
  font-size: ${styleVariables.fontSizes.giant};
`;
const AditionalText = styled.Text`
  font-size: ${styleVariables.fontSizes.bold};
  color: ${styleVariables.colors.whiteWeak};
`;
interface IProps {
  children: ReactNode;
  header: string;
  additionalText: string;
}
export const MenuItem: FC<IProps> = (props) => {
  const { children, header, additionalText } = props;
  return (
    <View
      style={{
        width: 700,
        height: 75,
        position: "relative",
      }}
    >
      <Ripple
        onTap={() => {
          console.log("tap");
        }}
      >
        <Container>
          <IconWrapper>{children}</IconWrapper>
          <TextWrapper>
            <HeaderText>{header}</HeaderText>
            <AditionalText>{additionalText}</AditionalText>
          </TextWrapper>
        </Container>
      </Ripple>
    </View>
  );
};
