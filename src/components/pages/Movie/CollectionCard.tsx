import { FC } from "react";
import styled from "styled-components/native";
import config from "../../../config";
import React from "react";
import { Dimensions, Image } from "react-native";
import { IGenre } from "../../../helpers/types/shared";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { IMovie } from "../../../helpers/types/movie";
import { Link } from "@react-navigation/native";
import { ENScreenGroups, ENScreens } from "../../../helpers/constants/routes";
import { AirbnbRating } from "react-native-ratings";

const Wrapper = styled.View`
  flex: 1;
  padding-left: 10px;
  margin-top: 50px;
`;
const Container = styled.View`
  max-height: 120px;
  width: ${Dimensions.get("window").width}px;
  padding-right: 10px;
`;
const ContentWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

const ImgWrapper = styled.View`
  height: 100px;
  width: 75px;
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteMain};
`;
const DescriptionWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  flex: 1;
  margin-left: 6px;
  margin-right: 6px;
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.biggest};
  font-size: ${styleVariables.fontSizes.biggest};
  letter-spacing: 0.1px;
  margin-bottom: 5px;
`;
const GenresContainer = styled.View`
  flex-direction: row;
  margin-left: 0px;
  margin-right: -5px;
`;
const Genre = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.medium};
  letter-spacing: 0.1px;
  margin-bottom: 6px;
  margin-right: 5px;
`;
const TextContainer = styled.View`
  align-items: flex-start;
  justify-content: flex-start;
`;
const VoteText = styled.Text`
  color: ${styleVariables.colors.greenBright};
  font-size: ${styleVariables.fontSizes.medium};
  font-weight: ${styleVariables.fontWeights.medium};
  margin-left: 5px;
`;
const RatingLine = styled.View`
  flex-direction: row;
`;
interface IProps {
  movie: IMovie;
}

export const CollectionCard: FC<IProps> = (props) => {
  const { id, title, poster_path, genre_ids, vote_average, vote_count } =
    props.movie;

  return (
    <Wrapper>
      <Link
        to={
          { screen: `${ENScreens.MOVIE}`, params: { id: id, pageName:title } }
        }
      >
        <Container>
          <ContentWrapper>
            <ImgWrapper>
              <Image
                resizeMode="cover"
                style={{ flex: 1 }}
                source={{ uri: config.apiImgUrl + poster_path }}
              />
            </ImgWrapper>
            <DescriptionWrapper>
              <TextContainer>
                <Title ellipsizeMode="tail">{title}</Title>
                <GenresContainer>
                  {genre_ids.map((el) => (
                    <Genre>{el}</Genre>
                  ))}
                </GenresContainer>
                <RatingLine>
                  <AirbnbRating
                    isDisabled={true}
                    showRating={false}
                    selectedColor={styleVariables.colors.greenBright}
                    count={5}
                    defaultRating={vote_average / 2}
                    size={10}
                  />
                  <VoteText>({vote_count})</VoteText>
                </RatingLine>
              </TextContainer>
              <AntDesign
                style={{ alignSelf: "center" }}
                color={styleVariables.colors.whiteWeak}
                name="right"
                size={15}
              />
            </DescriptionWrapper>
          </ContentWrapper>
        </Container>
      </Link>
    </Wrapper>
  );
};
