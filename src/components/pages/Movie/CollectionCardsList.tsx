import React from "react";
import { FC } from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { IMovie } from "../../../helpers/types/movie";
import { CollectionCard } from "./CollectionCard";

const Container = styled.SafeAreaView`
  flex-direction: column;
  margin-top: 20px;
`;
const Header = styled.Text`
  align-self: flex-end;
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  letter-spacing: 0.1px;
`;
const HeaderLine = styled.View`
  margin-left: 10px;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  margin-bottom: 15px;
`;

const ListWrapper = styled.View`
  margin-top: -50px;
  margin-bottom: 20px;
`;
interface IProps {
  crewList: IMovie[];
}

export const CollectionCardsList: FC<IProps> = (props) => {
  const { crewList } = props;
  return (
    <Container>
      <HeaderLine>
        <Header>This Collection Includes</Header>
      </HeaderLine>
      <ListWrapper>
        {crewList.map((el, i) => (
          <CollectionCard key={el.id} movie={el} />
        ))}
      </ListWrapper>
    </Container>
  );
};
