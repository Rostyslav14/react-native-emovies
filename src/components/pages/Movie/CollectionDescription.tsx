import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import { Image } from "react-native";
import config from "../../../config";
import { styleVariables } from "../../../styles/variables";

const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding-left: 10px;
  padding-right: 10px;
  margin-top: -25px;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  letter-spacing: 0.1px;
`;
const Description = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.biggest};
  font-size: ${styleVariables.fontSizes.bold};
  letter-spacing: 0.1px;
`;
const TextContainer = styled.View`
  flex: 2;
`;
const ImageContainer = styled.View`
  align-items: flex-end;
  flex: 1;
`;
const ImageWrapper = styled.View`
  width: 80px;
  height: 120px;
`;
interface IProps {
  text: string;
  title: string;
  imageUrl: string;
}
export const CollectionDescription: FC<IProps> = (props) => {
  const { text, title, imageUrl } = props;
  return (
    <Container>
      <TextContainer>
        <Header>{title}</Header>
        <Description>{text}</Description>
      </TextContainer>
      <ImageContainer>
        <ImageWrapper>
          <Image
            style={{ flex: 1 }}
            source={{ uri: config.apiImgUrl + imageUrl }}
          />
        </ImageWrapper>
      </ImageContainer>
    </Container>
  );
};
