import { FC } from "react";
import styled from "styled-components/native";
import config from "../../../config";
import React from "react";
import { Image, View } from "react-native";
import { IGenre } from "../../../helpers/types/shared";
import { ScrollView } from "react-native";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { IMovieBelongToCollection } from "../../../helpers/types/movie";
import { Link } from "@react-navigation/native";
import { ENScreens } from "../../../helpers/constants/routes";

const Wrapper = styled.View`
  margin-left: 10px;
  margin-top: 50px;
`;
const Container = styled.View`
  flex: 1;
  max-height: 120px;
  min-width:100%;
`;
const ContentWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  flex:1;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
  margin-bottom: 6px;
`;
const ImgWrapper = styled.View`
  height: 100px;
  width: 75px;
`;
const DescriptionWrapper = styled.View`
  flex-direction: row;
  justify-content: space-around;
  flex: 1;
  margin-left: 6px;
  margin-right: 6px;
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
  letter-spacing: 0.1px;
`;
const Genre = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.weak};
  font-size: ${styleVariables.fontSizes.medium};
  letter-spacing: 0.1px;
  margin-bottom: 6px;
`;
interface IProps {
  collection: IMovieBelongToCollection;
  genres: IGenre[];
}

export const CollectionsPreview: FC<IProps> = (props) => {
  const { id, name, poster_path, backdrop_path } = props.collection;
  const { genres } = props;
  return (
   <Wrapper>
     <Link style={{flex:1}} to={{ screen: `${ENScreens.MOVIE_COLLECTION}`, params: { id: id, pageName:name } }}>
      <Container>
        <Header>Collection</Header>
        <ContentWrapper>
          <ImgWrapper>
            <Image
              resizeMode="cover"
              style={{ flex: 1 }}
              source={{ uri: config.apiImgUrl + poster_path }}
            />
          </ImgWrapper>
          <DescriptionWrapper>
            <View>
              <Title>{name}</Title>
              <ScrollView horizontal={true}>
                <Genre>{[...genres].map((el) => el.name).join(", ")}</Genre>
              </ScrollView>
            </View>
            <AntDesign
              style={{ alignSelf: "center",flex:1 }}
              color={styleVariables.colors.whiteWeak}
              name="right"
              size={15}
            />
          </DescriptionWrapper>
        </ContentWrapper>
      </Container>
    </Link>
   </Wrapper>
  );
};
