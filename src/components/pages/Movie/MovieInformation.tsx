import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import React from "react";
import { ScrollView } from "react-native";
import { InformationItem } from "../../shared/InformationItem";
import { IProductionCompany } from "../../../helpers/types/shared";

const Container = styled.View`
  margin-left: 10px;
  margin-top: 50px;
  flex-direction: column;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;

type TProps = {
  language: string;
  releaseDate: string;
  budget: number;
  revenue: number;
  productionCompanies: IProductionCompany[];
};
export function MovieInformation(props: TProps) {
  const { language, releaseDate, budget, revenue, productionCompanies } = props;

  if (!arguments.length) {
    return <></>;
  }

  return (
    <Container>
      <Header>Information</Header>
      <ScrollView>
        {!!language && <InformationItem header={"Language"} value={language} />}
        {!!releaseDate && <InformationItem header={"Release Date"} value={releaseDate} />}
        {!!budget && <InformationItem header={"Budget"} value={budget} />}
        {!!revenue && <InformationItem header={"Revenue"} value={revenue} />}
        {!!productionCompanies && <InformationItem
          header={"Production Companies"}
          value={productionCompanies}
        />}
      </ScrollView>
    </Container>
  );
}
