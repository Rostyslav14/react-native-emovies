import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import Fontisto from 'react-native-vector-icons/Fontisto';
import { styleVariables } from "../../../styles/variables";

const Container = styled.View`
    flex-direction:row;
    flex:1;
    height:50px;
    align-items:center;
`
const Text = styled.Text`
    color:${styleVariables.colors.whiteWeak};
    font-size:${styleVariables.fontSizes.biggest};
    font-weight:${styleVariables.fontWeights.bold};
`
interface IProps {
    text:string
}
export const SearchRequest:FC<IProps> = (props)=>{
    const { text } = props
    return(

        <Container>
             <Fontisto
                name="search"
                size={18}
                color={styleVariables.colors.whiteWeak}
                style={{ marginLeft: 10,marginRight:20 }}
              />
              <Text>{text}</Text>

        </Container>
    )
}