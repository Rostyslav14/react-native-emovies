import React from "react";
import { FC } from "react";
import { FlatList } from "react-native-gesture-handler";
import { SearchRequest } from "./SearchRequest";

interface IProps {
  requests: string[];
}
export const SearchRequestsList: FC<IProps> = (props) => {
  const { requests } = props;
  return (
    <FlatList
      overScrollMode="never"
      data={requests}
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{ paddingHorizontal:10 }}
      showsVerticalScrollIndicator={false}
      renderItem={({ item }) => <SearchRequest text={item} />}
      keyExtractor={(item) => `${item}`}
    />
  );
};
