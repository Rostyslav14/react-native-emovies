import { ScrollView, TouchableOpacity, View } from "react-native";
import { FC } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import React from "react";
import { IMovie } from "../../../helpers/types/movie";
import { IActor } from "../../../helpers/types/actors";
import { ITVShow } from "../../../helpers/types/tvShow";

const Container = styled.View`
  padding-top:35px;
  flex: 1;
  padding-left: 10px;
  padding-right: 10px;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  height: 100%; ;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.extraGiant};
  letter-spacing: 0.1px;
  margin-bottom:5px;
`;
const ItemLine = styled.View`
  justify-content: center;
  align-items: center;
  text-align: center;
  flex: 1;
  margin-top:10px;
`;
const ItemText = styled.Text`
  text-align: center;
  color: ${styleVariables.colors.greenBright};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.giant};
  letter-spacing: 0.1px;
`;
interface IProps {
  trendings: Array<IMovie | ITVShow | IActor>;
}

export const Trendings: FC<IProps> = (props) => {
  const { trendings } = props;

  return (
    <Container>
      <Header>Trending</Header>
      <ScrollView>
        {trendings.map((el) => (
          <ItemLine key={el.id}>
            <TouchableOpacity>
              <ItemText>{el.title || el.name}</ItemText>
            </TouchableOpacity>
          </ItemLine>
        ))}
      </ScrollView>
    </Container>
  );
};
