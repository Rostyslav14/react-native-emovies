import { FC } from "react";
import styled from "styled-components/native";
import config from "../../../config";
import { Image } from "react-native";
import React from "react";
import { ISeason } from "../../../helpers/types/shared";
import { styleVariables } from "../../../styles/variables";
import { Link } from "@react-navigation/native";
import { ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View`
  flex-direction: column;
  align-items: flex-start;
  justify-content: center;
  width: 80px;
  margin-left: 10px;
  margin-right: 10px;
`;
const ImgWrapper = styled.View`
  width: 80px;
  height: 110px;
  border-width: 1px;
  border-color: ${styleVariables.colors.whiteMain};
`;
const TextWrapper = styled.View``;
const Season = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-size: ${styleVariables.fontSizes.bold}; ;
`;
interface IProps {
  season: ISeason;
  tvShowId: number;
  tvShowName: string;
}
export const SeasonCard: FC<IProps> = (props) => {
  const { season, tvShowId, tvShowName } = props;
  return (
    <Container>
      <Link
        to={{
          screen: `${ENScreens.TV_SHOW_SEASON}`,
          params: {
            pageName: 'Season '+season.season_number,
            tvShowName: tvShowName,
            tvId: tvShowId,
            seasonNumber: season.season_number,
          },
        }}
      >
        <ImgWrapper>
          <Image
            resizeMode="stretch"
            style={{
              flex: 1,
            }}
            source={{ uri: config.apiImgUrl + season.poster_path }}
          />
        </ImgWrapper>
        <TextWrapper>
          <Season>Season {season.season_number}</Season>
        </TextWrapper>
      </Link>
    </Container>
  );
};
