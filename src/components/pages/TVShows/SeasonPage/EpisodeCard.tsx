import React from "react";
import { FC } from "react";
import styled from "styled-components/native";
import { Image, View } from "react-native";
import config from "../../../../config";
import { styleVariables } from "../../../../styles/variables";
const Container = styled.View`
  flex: 1;
  padding-left: 10px;
  padding-right: 10px;
  flex-direction: row;
  margin-top: 10px;
  margin-bottom: 10px;
`;
const ImageBlock = styled.View`
  flex: 1;
`;
const TextBlock = styled.View`
  flex: 2;
  flex-direction: column;
  margin-left: 10px;
`;
const ImageWrapper = styled.View`
  max-height: 60px;
  flex: 1;
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteWeak};
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const DateText = styled.Text`
  color: rgb(185, 185, 185);
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const OverviewText = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
interface IProps {
  imgUrl: string;
  title: string;
  date: string;
  overview: string;
}
export const EpisodeCard: FC<IProps> = (props) => {
  const { imgUrl, title, date, overview } = props;
  return (
    <Container>
      <ImageBlock>
        <ImageWrapper>
          <Image
            resizeMode="stretch"
            style={{ flex: 1 }}
            source={{ uri: config.apiImgUrl + imgUrl }}
          />
        </ImageWrapper>
      </ImageBlock>
      <TextBlock>
        <Title>{title}</Title>
        <View>
          <DateText>{date}</DateText>
          <OverviewText>{overview}</OverviewText>
        </View>
      </TextBlock>
    </Container>
  );
};
