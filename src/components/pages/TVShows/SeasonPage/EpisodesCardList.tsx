import { FC } from "react";
import { ITVShowEpisode } from "../../../../helpers/types/tvShow";
import styled from "styled-components/native";
import React from "react";
import { FlatList, View } from "react-native";
import { EpisodeCard } from "./EpisodeCard";
import { styleVariables } from "../../../../styles/variables";

const Container = styled.View`
  flex-direction: column;
  flex: 1;
  margin-top:25px;
`;
const HeaderLine = styled.View`
  padding-right: 10px;
  padding-left: 10px;
  margin-bottom: 10px;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const EpisodesWrapper = styled.View`
  margin-top: -10px;
  margin-bottom: -10px;
`;
interface IProps {
  episodes: ITVShowEpisode[];
  showBackdropImg: string
}
export const EpisodesCardsList: FC<IProps> = (props) => {
  const { episodes,showBackdropImg } = props;
  return (
    <Container>
      <HeaderLine>
        <Header>Episodes</Header>
      </HeaderLine>
      <EpisodesWrapper>
        {episodes.map((el) => (
          <EpisodeCard
            imgUrl={!!el.still_path ? el.still_path : showBackdropImg}
            title={el.episode_number + "." + el.name}
            date={el.air_date}
            overview={el.overview}
          />
        ))}
      </EpisodesWrapper>
    </Container>
  );
};
