import { FC, ReactNode } from "react";
import { styleVariables } from "../../../../styles/variables";
import styled from "styled-components/native";
import React from "react";
import config from "../../../../config";
import { Image, View } from "react-native";
import { AirbnbRating } from "react-native-ratings";
import { DisabledRatingLine } from "../../../shared/DisabledRatingLine";

const Container = styled.View`
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 20px;
`;
const TopContainer = styled.View`
  flex-direction: row;
  flex: 1;
`;
const ImgWrapper = styled.View`
  width: 100px;
  height: 145px;
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteWeak};
  margin-right: 10px;
`;
const TextWrapper = styled.View`
  padding: 10px;
  flex: 1;
  flex-direction: column;
  justify-content: space-between;
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const Season = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const DateText = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const OverviewContainer = styled.View`
  margin-top: 15px;
`;
const OverviewHeader = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.biggest};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const Overview = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
interface IProps {
  imgUrl: string;
  seasonNumber: number;
  airingDate: string;
  title: string;
  children?: ReactNode;
  overview: string;
}
export const HeaderBlock: FC<IProps> = (props) => {
  const { imgUrl, seasonNumber, airingDate, title, children, overview } = props;
  return (
    <Container>
      <TopContainer>
        <ImgWrapper>
          <Image
            resizeMode="stretch"
            style={{
              flex: 1,
            }}
            source={{ uri: config.apiImgUrl + imgUrl }}
          />
        </ImgWrapper>
        <TextWrapper>
          <View>
            <Title>{title}</Title>
            <Season>Season {seasonNumber}</Season>
            <DateText>{airingDate}</DateText>
          </View>
          {children}
        </TextWrapper>
      </TopContainer>
      <OverviewContainer>
        <OverviewHeader>Abouth this Show</OverviewHeader>
        <Overview>{overview}</Overview>
      </OverviewContainer>
    </Container>
  );
};
