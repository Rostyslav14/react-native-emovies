import { FC } from "react";
import styled from "styled-components/native";
import { ScrollView } from "react-native";
import React from "react";
import { SeasonCard } from "./SeasonCard";
import { ISeason } from "../../../helpers/types/shared";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";

const Container = styled.View`
  margin-top: 20px;
  padding-right: 10px;
`;
const Header = styled.Text`
  align-self: flex-end;
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  letter-spacing: 0.1px;
`;
const HeaderLine = styled.View`
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  margin-bottom: 10px;
  padding-left: 10px;
`;

const FullListLink = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;

interface IProps {
  seasons: ISeason[];
  tvShowId: number;
  tvShowName: string;
}
export const SeasonCardsList: FC<IProps> = (props) => {
  const { seasons, tvShowId, tvShowName } = props;
  return (
    <Container>
      <HeaderLine>
        <Header>Seasons</Header>
        <FullListLink>
          See all <AntDesign name="right" size={10} />
        </FullListLink>
      </HeaderLine>
      <ScrollView
        overScrollMode="never"
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: -10 }}
      >
        {seasons.map((el) => (
          <SeasonCard key={el.id} tvShowName={tvShowName} tvShowId={tvShowId} season={el} />
        ))}
      </ScrollView>
    </Container>
  );
};
