import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import React from "react";
import { ScrollView } from "react-native";
import { InformationItem } from "../../shared/InformationItem";
import {
  ICreatedBy,
  INetwork,
  IProductionCompany,
} from "../../../helpers/types/shared";

const Container = styled.View`
  margin-left: 10px;
  margin-top: 50px;
  flex-direction: column;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;

type TProps = {
  createdBy: ICreatedBy[];
  airDate: string;
  language: string;
  originCountry: string[];
  networks: INetwork[];
};
export function TVInformation(props: TProps) {
  const { createdBy, airDate, language, originCountry, networks } = props;
  if (!arguments.length) {
    return <></>;
  }

  return (
    <Container>
      <Header>Information</Header>
      <ScrollView>
        {createdBy && (
          <InformationItem header={"Created by"} value={createdBy} />
        )}
        {airDate && (
          <InformationItem header={"First Air Date"} value={airDate} />
        )}
        {language && <InformationItem header={"Language"} value={language} />}
        {originCountry && (
          <InformationItem
            header={"Country of Origin"}
            value={originCountry[0]}
          />
        )}
        {networks && <InformationItem header={"Networks"} value={networks} />}
      </ScrollView>
    </Container>
  );
}
