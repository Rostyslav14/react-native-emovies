import React, { FC } from "react";
import { styleVariables } from "../../styles/variables";
import styled from "styled-components/native";
import { Pressable } from "react-native";

interface IProps {
  text: string;
  onPress: () => void;
  isDisabled: boolean;
  mainColor?: string;
}
const Btn = styled.View<{ isDisabled: boolean; mainColor?: string }>`
  background-color: ${(props) =>
    props.isDisabled
      ? styleVariables.colors.whiteOpacity
      : props.mainColor || styleVariables.colors.greenBright};
  align-self: center;
  justify-self: center;
  border-radius: 4px;
  padding: 5px 12px;
  min-width: 80px;
  align-items: center;
`;
const Text = styled.Text<{ isDisabled: boolean }>`
  color: ${(props) =>
    props.isDisabled
      ? styleVariables.colors.whiteOpacity
      : styleVariables.colors.greenMain};
  font-size: ${styleVariables.fontSizes.bold};
  font-weight: ${styleVariables.fontWeights.bold};
`;
export const BasicButton: FC<IProps> = (props) => {
  const { text, onPress, isDisabled, mainColor } = props;
  return (
    <Pressable disabled={isDisabled} onPress={onPress}>
      <Btn mainColor={mainColor} isDisabled={isDisabled}>
        {/* <Ripple
        style={{ paddingHorizontal: 10, paddingVertical: 5, borderRadius: 4 }}
        rippleDuration={800}
        rippleColor={styleVariables.colors.greenDeepDark}
      > */}
        <Text isDisabled={isDisabled}>{text}</Text>
        {/* </Ripple> */}
      </Btn>
    </Pressable>
  );
};
