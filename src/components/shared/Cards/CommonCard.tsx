import React from "react";
import { FC } from "react";
import { styleVariables } from "../../../styles/variables";
import styled from "styled-components/native";
import config from "../../../config";
import { TCardShapes, TCardSizes } from "../../../helpers/constants/ui";
import { Image, TouchableOpacity, View } from "react-native";
import { Link } from "@react-navigation/native";
import { ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View<{ size: TCardSizes }>`
  max-height: ${(props) => (props.size === "big" ? "175px" : "200px")};
  width: ${(props) => (props.size === "big" ? "200px" : "95px")};
`;
const ImgWrapper = styled.View<{ size: TCardSizes; shape: TCardShapes }>`
  height: 70%;
  width: 100%;
  border-width: 0.5px;
  border-radius: ${(props) => (props.shape === "rounded" ? "8px" : "0px")};
  border-color: ${styleVariables.colors.whiteMain};
`;
const Title = styled.Text<{ size: TCardSizes }>`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${(props) =>
    props.size === "big"
      ? styleVariables.fontWeights.medium
      : styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 17px;
  letter-spacing: 0.12px;
  margin-top: 8px;
`;
const GenresWrapper = styled.View`
  text-overflow: ellipsis;
  white-space: nowrap;

  overflow: hidden;
  flex: 1;
  flex-direction: row;
  margin-left: -5px;
  margin-right: -5px;
`;
const Genre = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.weak};
  font-size: ${styleVariables.fontSizes.weak};
  line-height: 12px;
  letter-spacing: 0.12px;
  margin-top: 4px;
  margin-left: 5px;
  margin-right: 5px;
`;
const DescriptionContainer = styled.View`
  height: 25%;
`;
interface IProps {
  id: number;
  imgUrl: string;
  title: string;
  additionalText: string[] | string | number[];
  cardVariants: {
    size: TCardSizes;
    shape: TCardShapes;
  };
  navigatePage: keyof typeof ENScreens;
}
export const CommonCard: FC<IProps> = (props) => {
  const { imgUrl, title, additionalText, cardVariants, id, navigatePage } =
    props;
  return (
    <TouchableOpacity>
      <Link
        to={{ screen: `${navigatePage}`, params: { id: id, pageName: title } }}
      >
        <Container size={cardVariants.size}>
          <ImgWrapper shape={cardVariants.shape} size={cardVariants.size}>
            <Image
              resizeMode="stretch"
              style={{
                flex: 1,
                borderRadius: cardVariants?.shape === "rounded" ? 8 : 0,
              }}
              source={{ uri: config.apiImgUrl + imgUrl }}
            />
          </ImgWrapper>
          <DescriptionContainer>
            <Title
              size={cardVariants.size}
              numberOfLines={1}
              ellipsizeMode="tail"
            >
              {title}
            </Title>
            <GenresWrapper>{Genres(additionalText)}</GenresWrapper>
          </DescriptionContainer>
        </Container>
      </Link>
    </TouchableOpacity>
  );
};

function Genres(genres: string | string[] | number[]) {
  if (Array.isArray(genres)) {
    return (
      <Genre numberOfLines={1} ellipsizeMode="tail">
        {genres.join(" ")}
      </Genre>
    );
  }
  return (
    <Genre numberOfLines={1} ellipsizeMode="tail">
      {genres}
    </Genre>
  );
}
