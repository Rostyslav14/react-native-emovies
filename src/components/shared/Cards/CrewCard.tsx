import { FC } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../../styles/variables";
import { Image } from "react-native";
import React from "react";
import config from "../../../config";
import { Ionicons } from "@expo/vector-icons";
import { Link } from "@react-navigation/native";
import { ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View`
  flex-direction: column;
  width: 110px;
  height: 120px;
  justify-content: center;
  align-items: center;
`;
const ImgWrapper = styled.View`
  width: 80px;
  height: 80px;
  border-color: ${styleVariables.colors.whiteMain};
  border-width: 1.5px;
  border-radius: 1000px;
  justify-content: center;
`;
const TextWrapper = styled.View`
  margin-top: 5px;
  flex: 1;
  justify-content: center;
  align-items: center;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.medium};
`;
const RoleText = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
`;
interface IProps {
  imgUrl: string;
  name: string;
  role?: string;
  id: number;
}
export const CrewCard: FC<IProps> = (props) => {
  const { imgUrl, name, role, id } = props;
  return (
    <Link to={{ screen: `${ENScreens.ACTOR}`, params: { id: id,pageName:name } }}>
      <Container>
        <ImgWrapper>
          {!!imgUrl ? (
            <Image
              style={{ flex: 1, borderRadius: 1000 }}
              resizeMode="cover"
              source={{ uri: config.apiImgUrl + imgUrl }}
            />
          ) : (
            <Ionicons
              name="person"
              color={styleVariables.colors.whiteWeak}
              size={50}
              style={{ alignSelf: "center" }}
            />
          )}
        </ImgWrapper>
        <TextWrapper>
          <Header numberOfLines={1}>{name}</Header>
          <RoleText numberOfLines={1}>{role}</RoleText>
        </TextWrapper>
      </Container>
    </Link>
  );
};
