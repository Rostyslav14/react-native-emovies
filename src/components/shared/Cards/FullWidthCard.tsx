import React, { ReactNode } from "react";
import { FC } from "react";
import { styleVariables } from "../../../styles/variables";
import styled from "styled-components/native";
import { Image, View, Text, Dimensions } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import { Link } from "@react-navigation/native";
import { ENScreens } from "../../../helpers/constants/routes";
import config from "../../../config";

const Container = styled.View`
  flex: 1;
  flex-direction: row;
  width: ${Dimensions.get("screen").width}px;
  padding-right: 10px;
  padding-left: 10px;
`;
const ImgWrapper = styled.View`
  height: 80px;
  width: 65px;
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteMain};
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.biggest};
  font-size: ${styleVariables.fontSizes.biggest};
  letter-spacing: 0.12px;
`;
const Genre = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 12px;
  letter-spacing: 0.12px;
  margin-top: 4px;
`;
const DescriptionContainer = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`;
const TextWrapper = styled.View`
  flex: 5;
  margin-left: 12px;
  justify-content: space-between;
`;
const IconWrapper = styled.View`
  flex: 2;
  justify-self: center;
  align-self: center;
  justify-content: flex-end;
  align-items: flex-end;
`;
interface IProps {
  imgUrl: string;
  title: string;
  additionalText: string[] | number[] | string;
  id: number;
  navigatePage: keyof typeof ENScreens;
  children?: ReactNode;
}
export const FullWidthCard: FC<IProps> = (props) => {
  const { imgUrl, title, additionalText, navigatePage, id, children } = props;
  return (
    <Link
      style={{ flex: 1 }}
      to={{ screen: `${navigatePage}`, params: { id: id, pageName: title } }}
    >
      <Container>
        <ImgWrapper>
          <Image
            resizeMode="cover"
            style={{
              flex: 1,
            }}
            source={{ uri: config.apiImgUrl + imgUrl }}
          />
        </ImgWrapper>
        <DescriptionContainer>
          <TextWrapper>
            <View>
              <Title numberOfLines={1} ellipsizeMode="tail">
                {title}
              </Title>
              <Genre numberOfLines={1} ellipsizeMode="tail">
                {!!Array.isArray(additionalText)
                  ? additionalText.join(" ")
                  : additionalText}
              </Genre>
            </View>
            <View>{children}</View>
          </TextWrapper>
          <IconWrapper>
            <AntDesign name="right" size={15} color={"white"} />
          </IconWrapper>
        </DescriptionContainer>
      </Container>
    </Link>
  );
};
