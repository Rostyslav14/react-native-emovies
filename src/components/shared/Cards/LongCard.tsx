import React from "react";
import { FC } from "react";
import { styleVariables } from "../../../styles/variables";
import styled from "styled-components/native";
import config from "../../../config";
import { TCardShapes, TCardSizes } from "../../../helpers/constants/ui";
import { Dimensions, Image, View } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import { Link } from "@react-navigation/native";
import { ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View`
  width: 280px;
  flex-direction: row;
`;
const ImgWrapper = styled.View<{ size: TCardSizes; shape: TCardShapes }>`
  height: ${(props) => (props.size === "big" ? 65 : 40)}px;
  width: ${(props) => (props.size === "big" ? 65 : 40)}px;
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteMain};
  border-radius: ${(props) =>
    props.shape === "rounded"
      ? Math.round(
          Dimensions.get("window").width + Dimensions.get("window").height
        ) / 2
      : 0}px;
`;
const Title = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
  line-height: 17px;
  letter-spacing: 0.12px;
  margin-top: 8px;
  margin-left: 12px;
`;
const Genre = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.weak};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 12px;
  letter-spacing: 0.12px;
  margin-top: 4px;
  margin-left: 12px;
`;
const DescriptionContainer = styled.View`
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  padding: 2px 0;
`;
const TextWrapper = styled.View`
  flex: 4;
`;
const IconWrapper = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  flex: 1;
`;
interface IProps {
  imgUrl: string;
  title: string;
  additionalText: string[] | string | number[];
  cardVariants: {
    size: TCardSizes;
    shape: TCardShapes;
  };
  id: number;
  navigatePage: keyof typeof ENScreens;
}
export const LongCard: FC<IProps> = (props) => {
  const { imgUrl, title, additionalText, cardVariants, navigatePage, id } =
    props;
  return (
    <Link to={{ screen: `${navigatePage}`, params: { id: id,pageName:title } }}>
      <Container>
        <ImgWrapper size={cardVariants.size} shape={cardVariants.shape}>
          <Image
            resizeMode="cover"
            style={{
              flex: 1,
              borderRadius:
                cardVariants.shape === "rounded"
                  ? Math.round(
                      Dimensions.get("window").width +
                        Dimensions.get("window").height
                    ) / 2
                  : 0,
            }}
            source={{ uri: config.apiImgUrl + imgUrl }}
          />
        </ImgWrapper>
        <DescriptionContainer>
          <TextWrapper>
            <Title numberOfLines={1} ellipsizeMode="tail">
              {title}
            </Title>
            <View>
              {AdditionalText(additionalText)}
            </View>
          </TextWrapper>
          <IconWrapper>
            <AntDesign name="right" size={12} color={"white"} />
          </IconWrapper>
        </DescriptionContainer>
      </Container>
    </Link>
  );
};

function AdditionalText(additionalText: string | string[] | number[]) {
  if (Array.isArray(additionalText)) {
    return (
      <Genre numberOfLines={1} ellipsizeMode="tail">
        {additionalText.join(" ")}
      </Genre>
    );
  }
  return (
    <Genre numberOfLines={1} ellipsizeMode="tail">
      {additionalText}
    </Genre>
  );
}
