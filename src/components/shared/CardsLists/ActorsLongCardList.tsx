import { FC } from "react";
import styled from "styled-components/native";
import { FlatList, TouchableOpacity, View } from "react-native";
import { LongCard } from "../Cards/LongCard";
import React from "react";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { IActor } from "../../../helpers/types/actors";
import { TCardShapes, TCardSizes } from "../../../helpers/constants/ui";
import { ENScreenGroups, ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View<{ size: TCardSizes }>`
  margin-top:25px;
  padding-bottom: 60px;
`;
const HeaderLine = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10px;
  padding-right: 20px;
`;
const ListTitle = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
  margin-bottom: 15px;
`;
const FullListLink = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;

function CardsCol({
  cardItems,
  cardVariants,
}: {
  cardItems: IActor[];
  cardVariants: any;
}) {
  return (
    <FlatList
      scrollEnabled={false}
      data={cardItems}
      overScrollMode="never"
      ItemSeparatorComponent={() => <View style={{ height: 6 }}></View>}
      renderItem={({ item }) => (
        <LongCard
          id={item.id}
          cardVariants={cardVariants}
          key={item.id}
          imgUrl={item.profile_path}
          title={item.name}
          additionalText={item.name}

          navigatePage={ENScreens.ACTOR}
        />
      )}
    />
  );
}
interface IProps {
  itemsList: IActor[][];
  listName: string;
  cardVariants: {
    size: TCardSizes;
    shape: TCardShapes;
  };
}
export const ActorsLongCardList: FC<IProps> = (props) => {
  const { itemsList, listName, cardVariants } = props;
  return (
    <Container size={cardVariants.size}>
      <HeaderLine>
        <ListTitle>{listName}</ListTitle>
        <TouchableOpacity>
          <FullListLink>
            See all <AntDesign name="right" size={10} />
          </FullListLink>
        </TouchableOpacity>
      </HeaderLine>
      <FlatList
        overScrollMode="never"
        data={itemsList}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        contentContainerStyle={{ paddingHorizontal: 10 }}
        ItemSeparatorComponent={() => <View style={{ width: 15 }} />}
        renderItem={({ item }) => (
          <CardsCol cardVariants={cardVariants} cardItems={item} />
        )}
        keyExtractor={(item) => `${item[0].id}`}
      />
    </Container>
  );
};
