import React from "react";
import { FC } from "react";
import { FlatList } from "react-native";
import styled from "styled-components/native";
import { CrewCard } from "../Cards/CrewCard";
import { ICastMember } from "../../../helpers/types/shared";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";

const Container = styled.View`
  flex-direction: column;
  margin-top: 15px;
`;
const Header = styled.Text`
  align-self: flex-end;
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  letter-spacing: 0.1px;
`;
const HeaderLine = styled.View`
  margin-left: 10px;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
  margin-bottom: 15px;
`;

const FullListLink = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;

interface IProps {
  crewList: ICastMember[];
}

export const CrewCardsList: FC<IProps> = (props) => {
  const { crewList } = props;
  return (
    <Container>
      <HeaderLine>
        <Header>Cast & Crew</Header>
        <FullListLink>
          See all <AntDesign name="right" size={10} />
        </FullListLink>
      </HeaderLine>
      <FlatList
        overScrollMode="never"
        data={crewList}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        contentContainerStyle={{ paddingHorizontal: 0 }}
        renderItem={({ item }) => (
          <CrewCard
            id={item.id}
            imgUrl={item.profile_path}
            name={item.name}
            role={item.character}
          />
        )}
        keyExtractor={(item) => `${item.id}`}
      />
    </Container>
  );
};
