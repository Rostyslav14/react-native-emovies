import { FC } from "react";
import styled from "styled-components/native";
import { FlatList, View } from "react-native";
import React from "react";
import { FullWidthCard } from "../../Cards/FullWidthCard";
import { DisabledRatingLine } from "../../DisabledRatingLine";
import { ITVShow } from "../../../../helpers/types/tvShow";

const Container = styled.View`
  margin-top: 15px;
  flex-direction: column;
  padding-bottom: 10px;
`;
interface IProps {
  itemsList: ITVShow[];
  onEndReached: () => void;
}

export const MovieFullWidthCardsList: FC<IProps> = (props) => {
  const { itemsList, onEndReached } = props;
  return (
    <Container>
      <FlatList
        overScrollMode="never"
        data={itemsList}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => <View style={{ height: 16 }} />}
        onEndReached={() => onEndReached()}
        renderItem={({ item }) => (
          <FullWidthCard
            navigatePage={"ACTORS"}
            id={item.id}
            title={item.name}
            additionalText={item.genre_ids}
            imgUrl={item.poster_path}
          >
            <DisabledRatingLine
              starSize={10}
              voteCount={item.vote_count}
              voteAverage={item.vote_average}
            />
          </FullWidthCard>
        )}
        keyExtractor={(item) => `${item.id}`}
      />
    </Container>
  );
};
