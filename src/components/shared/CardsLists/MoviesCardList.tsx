import { FC } from "react";
import styled from "styled-components/native";
import { FlatList, TouchableOpacity, View } from "react-native";
import { CommonCard } from "../Cards/CommonCard";
import React from "react";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { TCardShapes, TCardSizes } from "../../../helpers/constants/ui";
import { IMovie } from "../../../helpers/types/movie";
import { ENScreenGroups, ENScreens } from "../../../helpers/constants/routes";
import { Link } from "@react-navigation/native";
import { MOVIES_LIST_BY_ID_VARIANTS, MOVIES_LIST_REQUEST_VARIANTS } from "../../../helpers/constants/requestConstants";

const Container = styled.View`
  margin-top: 15px;
  flex-direction: column;
`;

const HeaderLine = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10px;
  padding-right: 5px;
  padding-bottom: 12px;
`;
const ListTitle = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const FullListLink = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
interface IProps {
  itemsList: IMovie[];
  listName: string;
  cardVariants: {
    size: TCardSizes;
    shape: TCardShapes;
  };
  listCategory: typeof MOVIES_LIST_REQUEST_VARIANTS[number] | typeof MOVIES_LIST_BY_ID_VARIANTS[number];
}

export const MoviesCardList: FC<IProps> = (props) => {
  const { itemsList, listName, cardVariants, listCategory } = props;
  return (
    <Container>
      <HeaderLine>
        <ListTitle>{listName}</ListTitle>
        <TouchableOpacity>
          <FullListLink>
            <Link
              to={{
                screen: ENScreens.FULL_ITEMS_LIST,
                params: { startingList: itemsList, listCategory: listCategory, pageName: listName },
              }}
            >
              See all <AntDesign name="right" size={8} />
            </Link>
          </FullListLink>
        </TouchableOpacity>
      </HeaderLine>
      <FlatList
        horizontal={true}
        overScrollMode="never"
        data={itemsList}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: 10 }}
        ItemSeparatorComponent={() => <View style={{ width: 16 }} />}
        renderItem={({ item }) => (
          <CommonCard
            navigatePage={ENScreens.MOVIE}
            id={item.id}
            cardVariants={cardVariants}
            key={item.id}
            imgUrl={
              cardVariants.size === "big"
                ? item.backdrop_path
                : item.poster_path
            }
            title={item.title}
            additionalText={item.genre_ids}
          />
        )}
        keyExtractor={(item) => `${item.id}`}
      />
    </Container>
  );
};
