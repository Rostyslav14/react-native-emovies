import { FC } from "react";
import styled from "styled-components/native";
import { FlatList, TouchableOpacity, View } from "react-native";
import { CommonCard } from "../Cards/CommonCard";
import React from "react";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { TCardShapes, TCardSizes } from "../../../helpers/constants/ui";
import { ITVShow } from "../../../helpers/types/tvShow";
import { ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View`
  margin-top: 15px;
  flex-direction: column;
`;

const HeaderLine = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10px;
  padding-right: 5px;
  padding-bottom: 12px;
`;
const ListTitle = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const FullListLink = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
interface IProps {
  itemsList: ITVShow[];
  listName: string;
  cardVariants: {
    size: TCardSizes;
    shape: TCardShapes;
  };
}

export const TVShowsCardList: FC<IProps> = (props) => {
  const { itemsList, listName, cardVariants } = props;
  return (
    <Container>
      <HeaderLine>
        <ListTitle>{listName}</ListTitle>
        <TouchableOpacity>
          <FullListLink>
            See all <AntDesign name="right" size={8} />
          </FullListLink>
        </TouchableOpacity>
      </HeaderLine>
      <FlatList
        horizontal={true}
        overScrollMode="never"
        data={itemsList}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: 10 }}
        ItemSeparatorComponent={() => <View style={{ width: 16 }} />}
        renderItem={({ item }) => (
          <CommonCard
            navigatePage={ENScreens.TV_SHOW}
            id={item.id}
            cardVariants={cardVariants}
            key={item.id}
            imgUrl={
              cardVariants.size === "big"
                ? item.backdrop_path
                : item.poster_path
            }
            title={item.name}
            additionalText={item.genre_ids}
          />
        )}
        keyExtractor={(item) => `${item.id}`}
      />
    </Container>
  );
};
