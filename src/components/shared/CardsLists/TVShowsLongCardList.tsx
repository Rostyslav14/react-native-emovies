import { FC } from "react";
import styled from "styled-components/native";
import { FlatList, TouchableOpacity, View } from "react-native";
import { LongCard } from "../Cards/LongCard";
import React from "react";
import { styleVariables } from "../../../styles/variables";
import AntDesign from "react-native-vector-icons/AntDesign";
import { TCardShapes, TCardSizes } from "../../../helpers/constants/ui";
import { ITVShow } from "../../../helpers/types/tvShow";
import { ENScreenGroups, ENScreens } from "../../../helpers/constants/routes";

const Container = styled.View`
  margin-top: 25px;
`;
const HeaderLine = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-left: 10px;
  padding-right: 20px;
`;
const ListTitle = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
  margin-bottom: 15px;
`;
const FullListLink = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.medium};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;

function CardsCol({
  cardItems,
  cardVariants,
}: {
  cardItems: ITVShow[];
  cardVariants: { size: TCardSizes; shape: TCardShapes };
}) {
  return (
    <FlatList
      scrollEnabled={false}
      data={cardItems}
      overScrollMode="never"
      ItemSeparatorComponent={() => <View style={{ height: 6 }}></View>}
      renderItem={({ item }) => (
        <LongCard
          id={item.id}
          cardVariants={cardVariants}
          key={item.id}
          imgUrl={item.poster_path}
          title={item.name}
          additionalText={item.genre_ids}
          navigatePage={ENScreens.TV_SHOW}
        />
      )}
    />
  );
}
interface IProps {
  itemsList: ITVShow[][];
  listName: string;
  cardVariants: {
    size: TCardSizes;
    shape: TCardShapes;
  };
}
export const TVShowsLongCardList: FC<IProps> = (props) => {
  const { itemsList, listName, cardVariants } = props;
  return (
    <Container>
      <HeaderLine>
        <ListTitle>{listName}</ListTitle>
        <TouchableOpacity>
          <FullListLink>
            See all <AntDesign name="right" size={10} />
          </FullListLink>
        </TouchableOpacity>
      </HeaderLine>
      <FlatList
        overScrollMode="never"
        data={itemsList}
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        contentContainerStyle={{ paddingHorizontal: 10 }}
        ItemSeparatorComponent={() => <View style={{ width: 15 }} />}
        renderItem={({ item }) => (
          <CardsCol cardVariants={cardVariants} cardItems={item} />
        )}
        keyExtractor={(item) => `${item[0].id}`}
      />
    </Container>
  );
};
