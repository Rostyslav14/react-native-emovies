import React, { FC, ReactNode } from "react";
import { Image, View } from "react-native";
import styled from "styled-components/native";
import config from "../../config";
import { IGenre } from "../../helpers/types/shared";
import { styleVariables } from "../../styles/variables";

const DescriptionWrapper = styled.View`
  padding: 0 5px;
  flex-direction: row;
  margin-top: -5px;
  flex: 1;
`;
const PosterImgWrapper = styled.View`
  width: 80px;
  height: 120px;
  margin-top: -10px;
`;
const GenreScroll = styled.ScrollView`
  flex-direction: row;
  padding-left: -4px;
  margin-right: -4px;
  margin-bottom: 6px;
`;
const GenreItem = styled.View`
  margin-left: 4px;
  margin-right: 4px;
  padding: 4px;
  border-width: 1px;
  flex-direction: row;
  border-color: ${styleVariables.colors.whiteWeak};
  justify-content: center;
  align-items: center;
`;
const SimpleText = styled.Text`
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.weak};
  font-size: ${styleVariables.fontSizes.medium};
  flex: 1;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
`;
const TextWrapper = styled.View`
  flex: 1;
  padding-right: 15px;
  margin-top: -10px;
  padding-left: 6px;
  flex-direction: column;
`;
const DescriptionTextContainer = styled.View`
  flex: 1;
  flex-direction: row;
  flex-wrap: wrap;
`;
const TextSeparator = styled.View`
  height: 6px;
`;

interface IProps {
  title: string;
  genres: IGenre[];
  description: string;
  posterImgUrl: string;
  children: ReactNode;

}

export const DetailsPreview: FC<IProps> = (props) => {
  const { title, genres, description, posterImgUrl,children } = props;
  return (
    <>
      <DescriptionWrapper>
        <PosterImgWrapper>
          <Image
            resizeMode="cover"
            source={{
              uri: config.apiImgUrl + posterImgUrl,
            }}
            style={{ flex: 1 }}
          />
        </PosterImgWrapper>
        <TextWrapper>
          <View>
            <Header>{title}</Header>
          </View>
          {children}
          <TextSeparator />
          <View>
            <GenreScroll
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              overScrollMode="never"
            >
              {genres.map((el) => (
                <GenreItem key={el.id}>
                  <SimpleText>{el.name}</SimpleText>
                </GenreItem>
              ))}
            </GenreScroll>
          </View>
          <DescriptionTextContainer style={{ flex: 1 }}>
            <SimpleText>{description}</SimpleText>
          </DescriptionTextContainer>
        </TextWrapper>
      </DescriptionWrapper>
    </>
  );
};
