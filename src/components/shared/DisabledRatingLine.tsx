import React from "react";
import { FC } from "react";
import { AirbnbRating } from "react-native-ratings";
import styled from "styled-components/native";
import { styleVariables } from "../../styles/variables";

const Line = styled.View`
  flex-direction: row;
  align-items: center;
`;
const CountText = styled.Text`
  margin-left: 5px;
  color: ${styleVariables.colors.greenBright};
  font-size: ${styleVariables.fontSizes.weak};
`;
interface IProps {
  starSize: number;
  voteCount: number;
  voteAverage: number;
}

export const DisabledRatingLine: FC<IProps> = (props) => {
  const { starSize, voteCount, voteAverage } = props;
  return (
    <Line>
      <AirbnbRating
        selectedColor={styleVariables.colors.greenBright}
        showRating={false}
        count={5}
        defaultRating={voteAverage / 2}
        size={starSize}
        isDisabled={true}
        starContainerStyle={{ width: 70, justifyContent: "space-evenly" }}
      />
      <CountText>({voteCount})</CountText>
    </Line>
  );
};
