import { Feather } from "@expo/vector-icons";
import React from "react";
import { FC } from "react";
import { Pressable } from "react-native";
import navigation from "../../navigation";
import { styleVariables } from "../../styles/variables";

interface IProps {
    navigation:any
}
export const GoBackArrowBtn:FC<IProps> = (props) => {
    const { navigation } = props
  return (
    <Pressable
      onPress={() => navigation.goBack()}
      style={({ pressed }) => ({
        opacity: pressed ? 0.5 : 1,
      })}
    >
      <Feather
        name="arrow-left"
        size={18}
        color={styleVariables.colors.greenBright}
        style={{ marginRight: 20 }}
      />
    </Pressable>
  );
};
