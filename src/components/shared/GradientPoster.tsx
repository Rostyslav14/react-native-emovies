import React, { FC } from "react";
import { ImageBackground } from "react-native";
import styled from "styled-components/native";
import { LinearGradient } from "expo-linear-gradient";

const Container = styled.View<{ height?: number }>`
  min-height: ${(props) => (props.height ? props.height : 200)}px;
`;
const BackdropImgWrapper = styled.View`
  flex: 1;
`;
interface IProps {
  ImgUrl: string;
  locations: number[];
  colors: string[];
  height?: number;
}

export const GradientPoster: FC<IProps> = (props) => {
  const { ImgUrl, height, colors, locations } = props;
  return (
    <Container height={height}>
      <BackdropImgWrapper>
        <ImageBackground
          resizeMode="cover"
          source={{
            uri: ImgUrl,
          }}
          style={{ flex: 1, justifyContent: "flex-end"}}
        >
          <LinearGradient
            style={{
              height: "25%",
            }}
            locations={locations}
            colors={colors}
          />
        </ImageBackground>
      </BackdropImgWrapper>
    </Container>
  );
};
