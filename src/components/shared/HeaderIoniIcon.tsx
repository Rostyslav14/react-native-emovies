import { FC, useState } from "react";
import { Ionicons } from "@expo/vector-icons";
import styled from "styled-components/native";
import React from "react";
import { styleVariables } from "../../../../styles/variables";
import { SafeAreaView } from "react-native-safe-area-context";

const IconContainer = styled.Pressable`
  justify-content: center;
  align-items: center;
  margin-left: 10px;
  margin-right: 10px;
  padding:3px 4px;
  background-color:red;
`;
interface IProps {
  activeName: keyof typeof Ionicons.glyphMap;
  unActiveName: keyof typeof Ionicons.glyphMap;
  isActive:boolean;
  onPress:()=>void;
}
export const HeaderIoniIcon: FC<IProps> = (props) => {
  const { activeName, unActiveName,isActive,onPress } = props;
  return (
    <IconContainer onPress={()=>onPress()}>
      <Ionicons
        size={22}
        color={styleVariables.colors.greenBright}
        name={isActive ? activeName : unActiveName}
      />
    </IconContainer>
  );
};
