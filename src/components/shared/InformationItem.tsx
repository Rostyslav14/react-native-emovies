import React from "react";
import { View } from "react-native";
import styled from "styled-components/native";
import { styleVariables } from "./../../styles/variables";
const TextLine = styled.View`
  margin-top:3px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const LineHeader = styled.Text`
  margin-right: 5px;
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.bold};
`;
const LineText = styled.Text`
  margin-left: 5px;
  color: ${styleVariables.colors.whiteWeak};
  font-weight: ${styleVariables.fontWeights.medium};
  font-size: ${styleVariables.fontSizes.bold};
`;

export function InformationItem({
  header,
  value,
}: {
  header: string;
  value: any;
}) {
  if (Array.isArray(value)) {
    return (
      <TextLine >
        <View
          style={{
            height:'100%',
            flex: 1,
            alignItems: "flex-end",
            justifyContent: "flex-start",

          }}
        >
          <LineHeader>{header}</LineHeader>
        </View>
        <View
          style={{
            flex: 1,
            alignItems: "flex-start",
            justifyContent: "flex-start",
            
          }}
        >
          {value.map((val) => (
            <LineText key={val.name} numberOfLines={2}>{val.name}</LineText>
          ))}
        </View>
      </TextLine>
    );
  }
  return (
    <TextLine>
      <View
        style={{
          flex: 1,
          alignItems: "flex-end",
          justifyContent: "flex-start",
        }}
      >
        <LineHeader numberOfLines={2}>{header}</LineHeader>
      </View>

      <View
        style={{
          flex: 1,
          alignItems: "flex-start",
          justifyContent: "flex-start",
        }}
      >
        <LineText>{value}</LineText>
      </View>
    </TextLine>
  );
}
