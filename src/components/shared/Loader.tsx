import React from "react";
import { FC } from "react";
import { ActivityIndicator, ActivityIndicatorProps } from "react-native";
import { styleVariables } from "./../../styles/variables";
import styled from "styled-components/native";
import { LAYOUT } from "./../../helpers/constants/layout";

const Container = styled.View`
  align-items: center;
  justify-content: center;
  min-width: ${LAYOUT.window.width}px;
  min-height: ${LAYOUT.window.height}px;
  background-color:${styleVariables.colors.greenMain};
`;

interface IProps extends ActivityIndicatorProps {}

export const Loader: FC<IProps> = (props) => {
  return (
    <Container>
      <ActivityIndicator {...props} color={styleVariables.colors.greenBright} size={'large'} />
    </Container>
  );
};
