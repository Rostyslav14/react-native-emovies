import React, { FC } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../styles/variables";
import { AirbnbRating } from "react-native-ratings";

const Container = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-right: 42px;
`;

const VoteCount = styled.Text`
  color: ${styleVariables.colors.greenBright};
  font-size: ${styleVariables.fontSizes.weak};
  font-weight: ${styleVariables.fontWeights.medium};
`;

const RatingGroup = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
const FullRating = styled.Text`
  color: ${styleVariables.colors.greenBright};
  font-size: ${styleVariables.fontSizes.medium};
  font-weight: ${styleVariables.fontWeights.medium};
`;
interface IProps {
  onRate?: () => void;
  rating: {
    average: number;
    count: number;
  };
  isDisabled?: boolean
}

export const RatingLine: FC<IProps> = (props) => {
  const { onRate, rating, isDisabled } = props;
  return (
    <>
      <Container>
        <RatingGroup>
          <AirbnbRating
          isDisabled
            showRating={false}
            selectedColor={styleVariables.colors.greenBright}
            count={5}
            defaultRating={rating.average / 2}
            size={8}
          />
          <VoteCount>( {rating.count} )</VoteCount>
        </RatingGroup>

        <RatingGroup>
          <AirbnbRating
            selectedColor={styleVariables.colors.greenBright}
            showRating={false}
            count={1}
            defaultRating={1}
            size={9}
            isDisabled
          />
          <FullRating>{rating.average}</FullRating>
        </RatingGroup>
      </Container>
    </>
  );
};
