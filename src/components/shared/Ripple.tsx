import React from "react";
import { FC, ReactNode } from "react";
import { View } from "react-native";
import {
  GestureHandlerRootView,
  TapGestureHandler,
  TapGestureHandlerGestureEvent,
} from "react-native-gesture-handler";
import Animated, {
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import { StyleProp, ViewStyle } from "react-native/types";
import { runOnJS } from "react-native-reanimated";

interface IProps {
  style?: StyleProp<ViewStyle>;
  onTap?: () => void;
  children?: ReactNode;
}
export const Ripple: FC<IProps> = (props) => {
  const { style, onTap, children } = props;
  const centerX = useSharedValue(0);
  const centerY = useSharedValue(0);
  const scale = useSharedValue(0);

  const tapGestureEvent =
    useAnimatedGestureHandler<TapGestureHandlerGestureEvent>({
      onStart: (tapEvent) => {
        console.log(tapEvent.x);
        centerX.value = tapEvent.x;
        centerY.value = tapEvent.y;
        scale.value = withTiming(1, { duration: 1000 });
      },
      onActive: (args) => {
        if (onTap) runOnJS(onTap)();
      },
      onEnd: () => {},
    });
  const rStyle = useAnimatedStyle(() => {
    const circleRadius = 20;
    const translateX = centerX.value;
    const translateY = centerY.value;
    console.log(translateX);
    console.log(translateY);
    return {
      width: circleRadius * 2,
      height: circleRadius * 2,
      borderRadius: circleRadius,
      backgroundColor: "blue",
      position: "absolute",

      left: 0,
      top: 0,
      opacity: 0.2,
      transform: [
        { translateX },
        { translateY },
        {
          scale: 1,
        },
      ],
    };
  });
  return (
    //gestureHandlerRootView - necessary for android/ on ios it's do nothing
    <GestureHandlerRootView>
      <TapGestureHandler onGestureEvent={tapGestureEvent}>
        <Animated.View style={style}>
          <View>{children}</View>
          <Animated.View style={rStyle}></Animated.View>
        </Animated.View>
      </TapGestureHandler>
    </GestureHandlerRootView>
  );
};
