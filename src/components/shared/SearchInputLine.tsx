import { FC, useEffect } from "react";
import styled from "styled-components/native";
import { styleVariables } from "../../styles/variables";
import React from "react";
import { TextInput } from "react-native-paper";
import Feather from "react-native-vector-icons/Feather";
import AntDesign from "react-native-vector-icons/AntDesign";


const Container = styled.View`
  background-color: ${styleVariables.colors.greenDeepDark};
  flex-direction: row;
  align-items: center;
`;

interface IProps {
  value: string;
}
export const SearchInputLine: FC<IProps> = (props) => {
  const { value } = props;

  return (
    <Container>
      <Feather
        name="arrow-left"
        size={18}
        color={styleVariables.colors.greenBright}
        style={{ marginLeft: 20 }}
      />
      <TextInput
        textColor={styleVariables.colors.whiteMain}
        placeholderTextColor={styleVariables.colors.whiteMain}
        underlineStyle={{ width: 0 }}
        activeUnderlineColor={styleVariables.colors.greenBright}
        style={{
          backgroundColor: "transparent",
          borderWidth: 0,
          fontSize: 20,
          fontWeight: "800",
          flex:1
        }}
        placeholder="Search"
      />
      <AntDesign name="close"
        size={18}
        color={styleVariables.colors.greenBright}
        style={{ marginRight: 20 }}/>
    </Container>
  );
};
