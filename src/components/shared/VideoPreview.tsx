import { FC } from "react";
import config from "./../../config";
import { Linking, Pressable } from "react-native";
import React from "react";
import styled from "styled-components/native";
import { styleVariables } from "./../../styles/variables";
import { AntDesign } from "@expo/vector-icons";
import { GradientPoster } from "./GradientPoster";

const Container = styled.View`
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteMain};
  width: 150px;
  height: 80px;
  border-width: 0.5px;
  border-color: ${styleVariables.colors.whiteMain};
  position: relative;
`;
const IconContainer = styled.View`
  position: absolute;
  right: 5px;
  bottom: 5px;
  background-color: ${styleVariables.colors.whiteMain};
  width: 22px;
  height: 17px;
  border-radius: 4px;
  justify-content: center;
  align-items: center;
`;

const HandleLikningToVideo = (videoId: string, movieName: string) => {
  Linking.canOpenURL("https://www.youtube.com/watch?v=" + videoId).then(
    (supported: boolean) => {
      if (supported) {
        return Linking.openURL("https://www.youtube.com/watch?v=" + videoId);
      } else {
        return Linking.openURL(
          "https://www.youtube.com/results?search_query=" + movieName
        );
      }
    }
  );
};
interface IProps {
  youtubeKey: string;
  movieName: string;
}

export const VideoPreview: FC<IProps> = (props) => {
  const { youtubeKey, movieName } = props;
  return (
    <Pressable onPress={() => HandleLikningToVideo(youtubeKey, movieName)}>
      <Container>
        <GradientPoster
          height={80}
          locations={[0, 0.9]}
          colors={["transparent", "black"]}
          ImgUrl={config.yotubeVideoPreviewUrl + youtubeKey + "/hqdefault.jpg"}
        />
        <IconContainer>
          <AntDesign name="youtube" color={"#FF0000"} size={18} />
        </IconContainer>
      </Container>
    </Pressable>
  );
};
