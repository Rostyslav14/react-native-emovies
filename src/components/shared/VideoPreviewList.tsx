import { FC } from "react";
import { IVideo } from "../../helpers/types/shared";
import { FlatList, View } from "react-native";
import React from "react";
import { VideoPreview } from "./VideoPreview";
import styled from "styled-components/native";
import { styleVariables } from "./../../styles/variables";

interface IProps {
  previewVideoItems: IVideo[];
}
const Container = styled.View`
  margin-left: 10px;
  margin-top: 50px;
`;
const Header = styled.Text`
  color: ${styleVariables.colors.whiteMain};
  font-weight: ${styleVariables.fontWeights.bold};
  font-size: ${styleVariables.fontSizes.giant};
  line-height: 19.5px;
  letter-spacing: 0.1px;
  margin-bottom: 10px;
`;
export const VideoPreviewList: FC<IProps> = (props) => {
  const { previewVideoItems } = props;

  return (
    <Container>
      <Header>Video</Header>
      <FlatList
        horizontal={true}
        overScrollMode="never"
        data={previewVideoItems}
        showsHorizontalScrollIndicator={false}
        ItemSeparatorComponent={() => <View style={{ width: 16 }} />}
        renderItem={({ item }) => (
          <VideoPreview movieName={item.name} youtubeKey={item.key} />
        )}
        keyExtractor={(item) => `${item.id}`}
      />
    </Container>
  );
};
