interface Config {
    apiUrl: string;
    apiKey: string;
    apiImgUrl: string;
    yotubeVideoPreviewUrl:string;
  }
  const config: Config = {
    apiUrl: 'https://api.themoviedb.org/3/',
    apiKey: '46e83f0897059a406661001f8d984825',
  
    apiImgUrl: 'https://image.tmdb.org/t/p/original/',
    yotubeVideoPreviewUrl:'https://img.youtube.com/vi/'
  };
  
  export default config;