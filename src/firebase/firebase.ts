



import { initializeApp, getApp, getApps } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyAwcZlcuOX0CQhZvojGUFXHmm1eCPtGlco",
  authDomain: "emovies-a7d40.firebaseapp.com",
  projectId: "emovies-a7d40",
  storageBucket: "emovies-a7d40.appspot.com",
  messagingSenderId: "1006027168904",
  appId: "1:1006027168904:web:303f3b36b9fdba6b75d33d",
  measurementId: "G-TK8TQV58XY"
}

// Initialize Firebase for SSR
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const firestore = getFirestore(app);
const auth = getAuth(app);
const storage = getStorage(app);

export { app, firestore, auth, storage };