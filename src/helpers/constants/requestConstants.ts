export const MOVIES_LIST_REQUEST_VARIANTS = [
  "top_rated",
  "upcoming",
  "popular",
  "now_playing",
  "latest",
] as const;
export const MOVIES_LIST_BY_ID_VARIANTS = [
  'similar',
  'recommendations'
] as const;

export const TVSHOWS_LIST_REQUEST_VARIANTS = [
  "popular",
  "top_rated",
  "on_the_air",
  "airing_today",
  "latest",
] as const;
export const TVSHOWS_LIST_BY_ID_VARIANTS = [
  'similar',
  'recommendations'
] as const;


export const SHARED_LIST_REQUEST_VARIANTS = [
  "all",
  "movie",
  "tv",
  "person",
] as const;

export const ACTORS_LIST_REQUEST_VARIANTS = ["popular", "latest"] as const;
