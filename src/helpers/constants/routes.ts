export enum ENScreens {
  TV_SHOWS = "TVSHOWS_HOME",
  TV_SHOW = "TVSHOW",
  TV_SHOW_SEASON = "TVSHOW_SEASON",
  ACTORS = "ACTORS_HOME",
  ACTOR = "ACTOR",
  CABINET = "CABINET_HOME",
  MOVIES = "MOVIES_HOME",
  MOVIE = "MOVIE",
  MOVIE_COLLECTION = "MOVIE_COLLECTION",
  SEARCH_PREVIEV = "SEARCH_PREVIEW",
  SEARCH_REQUEST_WRITE = "SEARCH_HOME",
  SEARCH_RESULTS_SHORT = "SEARCH_RESULTS_SHORT",
  SEARCH_RESULTS_FULL = "SEARCH_RESULTS_WITH_PREVIEW",
  FULL_ITEMS_LIST = "FULL_ITEMS_LIST",
  SIGN_UP = "SIGN_UP",
  SIGN_IN = "SIGN_IN",
  RATE_MODAL = "RATE_MODAL",
}

export enum ENScreenGroups {
  AUTH_GROUP = "AUTH",
  MOVIE_GROUP = "Movies",
  TV_GROUP = "Tv Shows",
  CELEBRETIES_GROUP = "Celebreties",
  SEARCH_GROUP = "Search",
  CABINET_GROUP = "Cabinet",
}
export type ENScreensGroupsValues =
  typeof ENScreenGroups[keyof typeof ENScreenGroups];

export type ENScreensValues = typeof ENScreens[keyof typeof ENScreens];
