export type TCardSizes = "normal" | "big";
export type TCardShapes = "rounded" | "square";
