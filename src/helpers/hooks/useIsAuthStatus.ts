import { onAuthStateChanged } from "firebase/auth";
import { auth } from "../../firebase/firebase";
import { useEffect, useState } from "react";

export const useIsAuthStatus = () => {
  const [status, setStatus] = useState<boolean>(false);
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        return setStatus(true);
      } else {
        return setStatus(false);
      }
    });
    return unsubscribe;
  }, []);
  return status;
};
