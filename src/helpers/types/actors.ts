import { IMovie } from "./movie";

export interface IActor {
  adult: boolean;
  also_known_as: IMovie[];
  biography: string;
  birthday: string;
  deathday: string;
  gender: number;
  optional: string;
  id: number;
  imdb_id: string;
  name: string;
  place_of_birth: string;
  popularity: number;
  profile_path: string;
}
export interface IActorDetails {
  birthday: string;
  known_for_department: string;
  deathday: string;
  id: number;
  name: string;
  also_known_as: string[];
  gender: 0 | 1 | 2 | 3;
  biography: string;
  popularity: number;
  place_of_birth: string;
  profile_path: string;
  adult: boolean;
  imdb_id: string;
  homepage: string;
}
