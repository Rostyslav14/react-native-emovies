import { IGenre, IProductionCompany, IProductionCountry } from "./shared";

export interface IMovie {
  poster_path: string;
  adult: boolean;
  overview: string;
  original_title: string;
  genre_ids: number[] | string[];
  id: number;
  media_type: string;
  original_language: string;
  title: string;
  backdrop_path: string;
  popularity: number;
  vote_count: number;
  video: boolean;
  vote_average: number;
}



export interface IMovieDetails extends Omit<IMovie, "genres_ids"> {
  adult: boolean;
  homepage: string;
  genres: IGenre[];
  runtime: number | null;
  imd_id: number;
  name: string;
  budget: number;
  belongs_to_collection: IMovieBelongToCollection | null;
  production_companies: IProductionCompany[];
  production_country: IProductionCountry[];
  release_date: string;
  revenue: number;
  status: string;
  tagline: string;
}

export interface IMovieBelongToCollection {
  id: number;
  name: string;
  poster_path: string;
  backdrop_path: string;
}
export interface IMovieCollection {
  id: number;
  name: string;
  poster_path: string;
  backdrop_path: string;
  overview:string
  parts:IMovie[]
}

