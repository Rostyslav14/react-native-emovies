export interface ICreatedBy {
  id: number;
  credit_id: string;
  name: string;
  gender: number;
  profile_path: string;
}
export interface ICastAndCrew {
  id:number
  cast:ICastMember[];
  crew:ICrewMember[];
}

export interface ICastMember {
  adult: boolean;
  gender: number;
  id: number;
  known_for_department: string;
  name: string;
  original_name: string;
  popularity: number;
  character: string;
  credit_id: number;
  order: string;
  profile_path:string
}
export interface ICrewMember extends Omit<ICastMember, "character" > {
  departament:string
  job:string

}
export interface IVideo {
  id: string;
  iso_639_1: string;
  iso_3166_1: string;
  key: string;
  name: string;
  official: boolean;
  published_at: string;
  site: string;
  size: number;
  type: string;
}
export interface IGenre {
  name: string;
  id: number;
}

export interface INetwork {
  name: string;
  id: number;
  logo_path: string;
  origin_country: string;
}
export interface ISpokenLanguage {
  english_name: string;
  iso_639_1: string;
  name: string;
}
export interface ISeason {
  air_date: string;
  episode_count: number;
  id: number;
  name: string;
  overview: string;
  poster_path: string;
  season_number: number;
}


export interface IProductionCompany {
  id: number;
  logo_path: string;
  name: string;
  origin_country: string;
}
export interface IProductionCountry {
  iso_3166_1: string;
  name: string;
}
export interface IVideo {
  id: string;
  iso_639_1: string;
  iso_3166_1: string;
  key: string;
  name: string;
  official: boolean;
  published_at: string;
  site: string;
  size: number;
  type: string;
}
