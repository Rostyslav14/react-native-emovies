export const DecodeISO6391 = (iso6391Lang: string): string =>
  new Intl.DisplayNames(["en"], { type: "language" }).of(iso6391Lang) || "";
