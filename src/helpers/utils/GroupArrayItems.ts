export function GroupArrayItems(items: any, groupSize: number) {
  let res = [];
  for (let i = 0; i < items.length; i += groupSize) {
    let slice = items.slice(i, i + groupSize);
    res.push(slice);
  }
  return res;
}
/// group array items in smaller arrays
