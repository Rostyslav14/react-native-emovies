import { IMovie } from "./../types/movie";
import { IGenre } from "./../types/shared";

export function parseGenres(encodes: IGenre[], movies: any[]) {
  //find ids in movies array
  let results: any[] = [];
  for (let el of movies) {
    results.push(parseSingleItemGenres(encodes, el));
  }
  return results;
}

function parseSingleItemGenres(encodes: IGenre[], movie: any) {
  let genres = [];
  for (let el of encodes) {
    for (let i = 0; i < movie.genre_ids.length; i++) {
      if (String(el.id) === String(movie.genre_ids[i])) {
        genres.push(el.name);
      }
    }
  }
  movie.genre_ids = genres;
  return movie;
}
