import React from "react";
import { ENScreens } from "../../helpers/constants/routes";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { SignUp } from "../../screens/Auth/SignUp";
import { Pressable } from "react-native";
import { MaterialIcons } from "@expo/vector-icons";
import { styleVariables } from "../../styles/variables";
import { SignIn } from "../../screens/Auth/SignIn";

const Stack = createNativeStackNavigator();

const screenOption = {
  headerStyle: { backgroundColor: styleVariables.colors.greenMain },
  headerShadowVisible: false,
  headerTitleStyle:{color:styleVariables.colors.greenBright}
};

export function AuthGroup() {
  return (
    <Stack.Navigator screenOptions={screenOption}>
      <Stack.Screen
        name={ENScreens.SIGN_UP}
        component={SignUp}
        options={({ navigation, route }: any) => ({
          title: "Sign Up",
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.goBack()}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <MaterialIcons
                name="close"
                size={20}
                style={{
                  marginRight: 20,
                  color: styleVariables.colors.greenBright,
                }}
              />
            </Pressable>
          ),
        })}
      />
       <Stack.Screen
        name={ENScreens.SIGN_IN}
        component={SignIn}
        options={({ navigation, route }: any) => ({
          title: "Sign In",
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.goBack()}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <MaterialIcons
                name="close"
                size={20}
                style={{
                  marginRight: 20,
                  color: styleVariables.colors.greenBright,
                }}
              />
            </Pressable>
          ),
        })}
      />
    </Stack.Navigator>
  );
}
