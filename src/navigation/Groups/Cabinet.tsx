import React from "react";
import { ENScreens } from "../../helpers/constants/routes";
import { styleVariables } from "../../styles/variables";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Cabinet } from "../../screens/Cabinet";

const Stack = createNativeStackNavigator();

const screenOption = {
  headerStyle: { backgroundColor: styleVariables.colors.greenDeepDark },
  headerTitleStyle: {
    color: styleVariables.colors.whiteMain,
  },
};

export function CabinetGroup() {
  return (
    <Stack.Navigator screenOptions={screenOption}>
      <Stack.Screen
        name={ENScreens.CABINET}
        component={Cabinet}
        options={{ title: "TMDB", headerShown: false }}
      />
    </Stack.Navigator>
  );
}
