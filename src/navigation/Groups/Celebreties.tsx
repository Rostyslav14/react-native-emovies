import { Feather } from "@expo/vector-icons";
import React from "react";
import { Pressable } from "react-native";
import { ENScreens } from "../../helpers/constants/routes";
import { styleVariables } from "../../styles/variables";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Actor } from "../../screens/Actor/Actor";
import { Actors } from "../../screens/Actor/Actors";

const Stack = createNativeStackNavigator();

const screenOption = {
  headerStyle: { backgroundColor: styleVariables.colors.greenDeepDark },
  headerTitleStyle: {
    color: styleVariables.colors.whiteMain,
  },
};

export function CelebretiesGroup() {
  return (
    <Stack.Navigator screenOptions={screenOption}>
      <Stack.Screen
        name={ENScreens.ACTORS}
        component={Actors}
        options={{
          title: 'Actors',
        }}
      />
      <Stack.Screen
        name={ENScreens.ACTOR}
        component={Actor}
        options={({ navigation,route }:any) => ({
          title: route.params?.pageName,
          tabBarItemStyle: { display: "none" },
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.goBack()}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <Feather
                name="arrow-left"
                size={18}
                color={styleVariables.colors.greenBright}
                style={{ marginRight: 20 }}
              />
            </Pressable>
          ),
        })}
      />
    </Stack.Navigator>
  );
}
