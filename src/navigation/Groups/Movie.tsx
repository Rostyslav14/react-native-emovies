import { Feather } from "@expo/vector-icons";
import React from "react";
import { Pressable } from "react-native";
import { ENScreens } from "../../helpers/constants/routes";
import { Movie } from "../../screens/Movie/Movie";
import { MovieCollection } from "../../screens/Movie/MovieCollection";
import { Movies } from "../../screens/Movie/Movies";
import { styleVariables } from "../../styles/variables";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { FullMoviesList } from "../../screens/Movie/FullMoviesList";
import { HeaderWithBackBtn } from "../../components/layout/Headers/HeaderWitchBackBtn";

const Stack = createNativeStackNavigator();

const screenOption = {
  headerStyle: { backgroundColor: styleVariables.colors.greenDeepDark },
  headerTitleStyle: {
    color: styleVariables.colors.whiteMain,
  },
};

export function MovieGroup() {
  return (
    <Stack.Navigator screenOptions={screenOption}>
      <Stack.Screen
        name={ENScreens.MOVIES}
        component={Movies}
        options={({ navigation }) => ({
          title: "Movies",
        })}
      />
      <Stack.Screen
        name={ENScreens.MOVIE}
        component={Movie}
        options={({ navigation, route }: any) => ({
          title: route.params?.pageName,
          headerShown: false,
        })}
      />
      <Stack.Screen
        name={ENScreens.MOVIE_COLLECTION}
        component={MovieCollection}
        options={({ navigation, route }: any) => ({
          title: route.params?.pageName,
          header: () => (
            <HeaderWithBackBtn
              navigation={navigation}
              title={route.params?.pageName}
            />
          ),
        })}
      />
      <Stack.Screen
        name={ENScreens.FULL_ITEMS_LIST}
        component={FullMoviesList}
        options={({ navigation, route }: any) => ({
          title: route.params?.pageName,
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.goBack()}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <Feather
                name="arrow-left"
                size={18}
                color={styleVariables.colors.greenBright}
                style={{ marginRight: 20 }}
              />
            </Pressable>
          ),
        })}
      />
    </Stack.Navigator>
  );
}
