import { Feather, Fontisto } from "@expo/vector-icons";
import React from "react";
import { Pressable } from "react-native";
import { ENScreens } from "../../helpers/constants/routes";
import { styleVariables } from "../../styles/variables";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { SearchPreview } from "../../screens/Search/SearchPreview";
import { SearchRequestWrite } from "../../screens/Search/SearchRequestWrite";

const Stack = createNativeStackNavigator();

const screenOption = {
  headerStyle: { backgroundColor: styleVariables.colors.greenDeepDark },
  headerTitleStyle: {
    color: styleVariables.colors.whiteMain,
  },
};

export function SearchGroup() {
  return (
    <Stack.Navigator screenOptions={screenOption}>
      <Stack.Screen
        name={ENScreens.SEARCH_PREVIEV}
        component={SearchPreview}
        options={({ navigation }) => ({
          title: 'Search',
          headerRight: () => (
            <Pressable
              onPress={() =>
                navigation.navigate(ENScreens.SEARCH_REQUEST_WRITE)
              }
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <Fontisto
                name="search"
                size={18}
                color={styleVariables.colors.greenBright}
                style={{ marginRight: 10 }}
              />
            </Pressable>
          ),
        })}
      />
      <Stack.Screen
        name={ENScreens.SEARCH_REQUEST_WRITE}
        component={SearchRequestWrite}
        options={({ navigation }) => ({
          tabBarItemStyle: { display: "none" },
          headerShown:false
        })}
      />
    </Stack.Navigator>
  );
}
