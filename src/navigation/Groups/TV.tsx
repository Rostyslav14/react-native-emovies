import { Feather } from "@expo/vector-icons";
import React from "react";
import { Pressable } from "react-native";
import { ENScreens } from "../../helpers/constants/routes";
import { styleVariables } from "../../styles/variables";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { TVShow } from "../../screens/TVShow/TVShow";
import { TVShowSeason } from "../../screens/TVShow/TVShowSeason";
import { TVShows } from "../../screens/TVShow/TVShows";

const Stack = createNativeStackNavigator();

const screenOption = {
  headerStyle: { backgroundColor: styleVariables.colors.greenDeepDark },
  headerTitleStyle: {
    color: styleVariables.colors.whiteMain,
  },
};

export function TVGroup() {
  return (
    <Stack.Navigator screenOptions={screenOption}>
      <Stack.Screen
        name={ENScreens.TV_SHOWS}
        component={TVShows}
        options={{
          title: 'TV Shows',
        }}
      />
      <Stack.Screen
        name={ENScreens.TV_SHOW}
        component={TVShow}
        options={({ navigation,route }:any) => ({
          title: route.params?.pageName,
          tabBarItemStyle: { display: "none" },
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.goBack()}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <Feather
                name="arrow-left"
                size={18}
                color={styleVariables.colors.greenBright}
                style={{ marginRight: 20 }}
              />
            </Pressable>
          ),
        })}
      />
      <Stack.Screen
        name={ENScreens.TV_SHOW_SEASON}
        component={TVShowSeason}
        options={({ navigation,route }:any) => ({
          title: route.params?.pageName,
          tabBarItemStyle: { display: "none" },
          headerLeft: () => (
            <Pressable
              onPress={() => navigation.goBack()}
              style={({ pressed }) => ({
                opacity: pressed ? 0.5 : 1,
              })}
            >
              <Feather
                name="arrow-left"
                size={18}
                color={styleVariables.colors.greenBright}
                style={{ marginRight: 20 }}
              />
            </Pressable>
          ),
        })}
      />
    </Stack.Navigator>
  );
}
