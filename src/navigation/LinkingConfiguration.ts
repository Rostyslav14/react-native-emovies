import { SearchPreview } from '../screens/Search/SearchPreview';
/**
 * Learn more about deep linking with React Navigation
 * https://reactnavigation.org/docs/deep-linking
 * https://reactnavigation.org/docs/configuring-links
 */

import { LinkingOptions } from "@react-navigation/native";
import * as Linking from "expo-linking";
import { ENScreens } from '../helpers/constants/routes';

export const linking: LinkingOptions<any> = {
  prefixes: [Linking.createURL("/")],
  config: {
    screens: {
      Root: {
        screens: {
          Movies: {
            screens: {
              MoviesScreen: ENScreens.MOVIES,
              MovieScreen:ENScreens.MOVIE
            },
          },
          TVShows: {
            screens: {
              TVShowsScreen: ENScreens.TV_SHOWS,
              TVShowScreen: ENScreens.TV_SHOW
            },
          },
          Celebreties: {
            screens: {
              ActorsScreen: ENScreens.ACTORS,
            },
          },
          Search: {
            screens: {
              SearchPreview: ENScreens.SEARCH_PREVIEV,
              SearchRequestWrite: ENScreens.SEARCH_REQUEST_WRITE,
              SearchResultsShort: ENScreens.SEARCH_RESULTS_SHORT,
              SearchResultsFull: ENScreens.SEARCH_RESULTS_FULL
            },
          },
          Cabinet: {
            screens: {
              CabinetScreen: ENScreens.CABINET,
            },
          },
        },
      },
      FullListModal:'FullListModal',
      Movie:'Movie',
      NotFound: "*",
    },
  },
};

