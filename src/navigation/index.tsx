import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import React from "react";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import Feather from "react-native-vector-icons/Feather";
import Ionicons from "react-native-vector-icons/Ionicons";
import Entypo from "react-native-vector-icons/Entypo";
import { NotFoundScreen } from "../screens/NotFoundScreen";
import { styleVariables } from "../styles/variables";
import { ENScreenGroups, ENScreens } from "../helpers/constants/routes";
import { linking } from "./LinkingConfiguration";
import { MovieGroup } from "./Groups/Movie";
import { TVGroup } from "./Groups/TV";
import { CelebretiesGroup } from "./Groups/Celebreties";
import { CabinetGroup } from "./Groups/Cabinet";
import { SearchGroup } from "./Groups/Search";
import { AuthGroup } from "./Groups/Auth";
import { RateModal } from "../screens/RateModal";
import { GoBackArrowBtn } from "../components/shared/GoBackArrowBtn";

export default function Navigation() {
  return (
    <NavigationContainer linking={linking}>
      <RootNavigator />
    </NavigationContainer>
  );
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */
const Stack = createNativeStackNavigator();

function RootNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Root"
        component={BottomTabNavigator}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="NotFound"
        component={NotFoundScreen}
        options={{ title: "Oops!" }}
      />
      <Stack.Screen
        name={ENScreenGroups.AUTH_GROUP}
        component={AuthGroup}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name={ENScreens.RATE_MODAL}
        component={RateModal}
        options={({ navigation, route }: any) => ({
          title:'',
          headerStyle:{backgroundColor:styleVariables.colors.greenDeepDark},
          headerLeft: () => <GoBackArrowBtn navigation={navigation} />,
        })}
      />

      {/* <Stack.Group screenOptions={{ presentation: "modal" }}>
        <Stack.Screen name="FullListModal" component={FullListModal} />
      </Stack.Group> */}
    </Stack.Navigator>
  );
}

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator();

const screenOptions = {
  headerShown: false,

  tabBarActiveTintColor: styleVariables.colors.greenBright,
  tabBarStyle: {
    backgroundColor: styleVariables.colors.greenWeak,
    borderTopWidth: 0,
    paddingBottom: 5,
    paddingTop: 5,
  },
};

function BottomTabNavigator() {
  return (
    <BottomTab.Navigator
      initialRouteName={ENScreenGroups.AUTH_GROUP}
      screenOptions={screenOptions}
    >
      <BottomTab.Screen
        name={ENScreenGroups.MOVIE_GROUP}
        component={MovieGroup}
        options={{
          tabBarIcon: ({ color }) =>
            TabBarIcon(ENScreenGroups.MOVIE_GROUP, 15, color),
        }}
      />
      <BottomTab.Screen
        name={ENScreenGroups.TV_GROUP}
        component={TVGroup}
        options={{
          tabBarIcon: ({ color }) =>
            TabBarIcon(ENScreenGroups.TV_GROUP, 15, color),
        }}
      />
      <BottomTab.Screen
        name={ENScreenGroups.CELEBRETIES_GROUP}
        component={CelebretiesGroup}
        options={{
          tabBarIcon: ({ color }) =>
            TabBarIcon(ENScreenGroups.CELEBRETIES_GROUP, 15, color),
        }}
      />
      <BottomTab.Screen
        name={ENScreenGroups.SEARCH_GROUP}
        component={SearchGroup}
        options={{
          tabBarIcon: ({ color }) =>
            TabBarIcon(ENScreenGroups.SEARCH_GROUP, 15, color),
        }}
      />

      <BottomTab.Screen
        name={"TMDB"}
        component={CabinetGroup}
        options={{
          tabBarIcon: ({ color }) =>
            TabBarIcon(ENScreenGroups.CABINET_GROUP, 15, color),
        }}
      />
    </BottomTab.Navigator>
  );
}

/**
 * You can explore the built-in icon families and icons on the web at https://icons.expo.fyi/
 */
function TabBarIcon(screenName: string, labelSize: number, color: string) {
  switch (screenName) {
    case ENScreenGroups.MOVIE_GROUP:
      return (
        <MaterialIcons color={color} name="local-movies" size={labelSize} />
      );
    case ENScreenGroups.TV_GROUP:
      return <Feather color={color} name="tv" size={labelSize} />;
    case ENScreenGroups.CELEBRETIES_GROUP:
      return <Ionicons color={color} name="person" size={labelSize} />;
    case ENScreenGroups.SEARCH_GROUP:
      return <Feather color={color} name="search" size={labelSize} />;
    case ENScreenGroups.CABINET_GROUP:
      return (
        <MaterialCommunityIcons
          color={color}
          name="account-box"
          size={labelSize}
        />
      );
    case "Movie":
      return <></>;
    default:
      return <Entypo color={color} name="yelp" size={labelSize} />;
  }
}
