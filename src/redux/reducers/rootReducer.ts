import { combineReducers } from "redux";
import { controlsReducer } from "../slices/controlsSlice";
import { moviesReducer } from "../slices/moviesSlice";
import { cabinetReducer } from "../slices/cabinetSlice";

const rootReducer = combineReducers({
  controlsSlicer: controlsReducer,
  moviesSlicer: moviesReducer,
  cabinetSlicer: cabinetReducer,
});
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
