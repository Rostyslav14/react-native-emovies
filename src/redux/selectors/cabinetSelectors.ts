import { ITVShowDetails } from "./../../helpers/types/tvShow";
import { IMovieDetails } from "../../helpers/types/movie";
import { createSelector } from "@reduxjs/toolkit";
import { RootState } from "../store";

const selectFavorite = (store: RootState) => store.cabinetSlicer.favorites;
const selectWatchList = (store: RootState) => store.cabinetSlicer.watchList;
const selectRated = (store: RootState) => store.cabinetSlicer.rated;
const selectUserRatings = (store: RootState) => store.cabinetSlicer.userRatings;

const selectIsMovieFavorite = createSelector(
  [selectFavorite, (_, movieId) => movieId],
  (group, movieId) =>
    group.movies.some((movie: IMovieDetails) => movie.id === movieId)
);
const selectIsMovieInWatchList = createSelector(
  [selectWatchList, (_, movieId) => movieId],
  (group, movieId) =>
    group.movies.some((movie: IMovieDetails) => movie.id === movieId)
);
const selectIsMovieRated = createSelector(
  [selectRated, (_, movieId) => movieId],
  (group, movieId) =>
    group.movies.some((movie: IMovieDetails) => movie.id === movieId)
);
const selectUserMovieRating = createSelector(
  [selectUserRatings, (_, movieId) => movieId],
  (group, movieId) => group.movies.find((movie) => movie.id === movieId)?.rating
);

const selectIsTVShowFavorite = createSelector(
  [selectFavorite, (_, TVShowId) => TVShowId],
  (group, TVShowId) =>
    group.TVShows.some((TVShow: ITVShowDetails) => TVShow.id === TVShowId)
);
const selectIsTVShowInWatchList = createSelector(
  [selectWatchList, (_, TVShowId) => TVShowId],
  (group, TVShowId) =>
    group.TVShows.some((TVShow: ITVShowDetails) => TVShow.id === TVShowId)
);
const selectIsTVShowRated = createSelector(
  [selectRated, (_, TVShowId) => TVShowId],
  (group, TVShowId) =>
    group.TVShows.some((TVShow: ITVShowDetails) => TVShow.id === TVShowId)
);

export const cabinetSelectors = {
  movies: {
    isFavorite: selectIsMovieFavorite,
    isInWatchList: selectIsMovieInWatchList,
    isRated: selectIsMovieRated,
    findUserMovieRating:selectUserMovieRating
  },
  tvShows: {
    isFavorite: selectIsTVShowFavorite,
    isInWatchList: selectIsTVShowInWatchList,
    isRated: selectIsTVShowRated,
  },
};
