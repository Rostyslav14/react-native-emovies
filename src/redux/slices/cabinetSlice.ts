import { IMovieDetails } from "./../../helpers/types/movie";
import { ITVShowDetails } from "./../../helpers/types/tvShow";
import { PayloadAction, createSlice } from "@reduxjs/toolkit";

interface ICabinetCategory {
  movies: IMovieDetails[];
  TVShows: ITVShowDetails[];
}
export interface ICabinetState {
  favorites: ICabinetCategory;
  watchList: ICabinetCategory;
  rated: ICabinetCategory;
  userRatings: {
    movies: { id: number; rating: number }[];
    TVShows: { id: number; rating: number }[];
  };
}

const initialState: ICabinetState = {
  favorites: {
    movies: [],
    TVShows: [],
  },
  watchList: {
    movies: [],
    TVShows: [],
  },
  rated: {
    movies: [],
    TVShows: [],
  },
  userRatings: {
    movies: [],
    TVShows: [],
  },
};

const cabinetSlice = createSlice({
  name: "cabinet",
  initialState,
  reducers: {
    addFavoriteMovie: (
      state,
      action: PayloadAction<{ movie: IMovieDetails }>
    ) => {
      state.favorites = {
        ...state.favorites,
        movies: [...state.favorites.movies, action.payload.movie],
      };
    },
    removeFavoriteMovie: (
      state,
      action: PayloadAction<{ movieId: number }>
    ) => {
      state.favorites = {
        ...state.favorites,
        movies: [
          ...state.favorites.movies.filter(
            (movie) => movie.id !== action.payload.movieId
          ),
        ],
      };
    },
    addFavoriteTVShow: (
      state,
      action: PayloadAction<{ tvShow: ITVShowDetails }>
    ) => {
      state.favorites = {
        ...state.favorites,
        TVShows: [...state.favorites.TVShows, action.payload.tvShow],
      };
    },
    removeFavoriteTVShow: (
      state,
      action: PayloadAction<{ tvShowId: number }>
    ) => {
      state.favorites = {
        ...state.favorites,
        TVShows: [
          ...state.favorites.TVShows.filter(
            (TVShow) => TVShow.id !== action.payload.tvShowId
          ),
        ],
      };
    },
    addRatedMovie: (
      state,
      action: PayloadAction<{ movie: IMovieDetails; userRating: number }>
    ) => {
      state.rated = {
        ...state.rated,
        movies: [...state.rated.movies, action.payload.movie],
      };
      state.userRatings = {
        ...state.userRatings,
        movies: [
          ...state.userRatings.movies.filter(
            (el) => el.id !== action.payload.movie.id
          ),
          { id: action.payload.movie.id, rating: action.payload.userRating },
        ],
      };
    },
    removeRatedMovie: (state, action: PayloadAction<{ movieId: number }>) => {
      state.rated = {
        ...state.rated,
        movies: [
          ...state.rated.movies.filter(
            (movie) => movie.id !== action.payload.movieId
          ),
        ],
      };
      state.userRatings = {
        ...state.userRatings,
        movies: state.userRatings.movies.filter(
          (movie) => movie.id !== action.payload.movieId
        ),
      };
    },
    addRatedTVShow: (
      state,
      action: PayloadAction<{ tvShow: ITVShowDetails; userRating: number }>
    ) => {
      state.rated = {
        ...state.rated,
        TVShows: [...state.rated.TVShows, action.payload.tvShow],
      };
      state.userRatings = {
        ...state.userRatings,
        TVShows: [
          ...state.userRatings.TVShows,
          { id: action.payload.tvShow.id, rating: action.payload.userRating },
        ],
      };
    },
    removeRatedTVShow: (state, action: PayloadAction<{ tvShowId: number }>) => {
      state.rated = {
        ...state.rated,
        TVShows: [
          ...state.rated.TVShows.filter(
            (TVShow) => TVShow.id !== action.payload.tvShowId
          ),
        ],
      };
      state.userRatings = {
        ...state.userRatings,
        TVShows: state.userRatings.TVShows.filter(
          (TVShow) => TVShow.id !== action.payload.tvShowId
        ),
      };
    },
    addWatchListMovie: (
      state,
      action: PayloadAction<{ movie: IMovieDetails }>
    ) => {
      state.watchList = {
        ...state.watchList,
        movies: [...state.watchList.movies, action.payload.movie],
      };
    },
    removeWatchListMovie: (
      state,
      action: PayloadAction<{ movieId: number }>
    ) => {
      state.watchList = {
        ...state.watchList,
        movies: [
          ...state.watchList.movies.filter(
            (movie) => movie.id !== action.payload.movieId
          ),
        ],
      };
    },
    addWatchListTVShow: (
      state,
      action: PayloadAction<{ tvShow: ITVShowDetails }>
    ) => {
      state.watchList = {
        ...state.watchList,
        TVShows: [...state.watchList.TVShows, action.payload.tvShow],
      };
    },
    removeWatchListTVShow: (
      state,
      action: PayloadAction<{ tvShowId: number }>
    ) => {
      state.watchList = {
        ...state.watchList,
        TVShows: [
          ...state.watchList.TVShows.filter(
            (TVShow) => TVShow.id !== action.payload.tvShowId
          ),
        ],
      };
    },
  },
  extraReducers: (builder) => {},
});

export const { reducer: cabinetReducer, actions: cabinetActions } =
  cabinetSlice;
