import { createSlice } from "@reduxjs/toolkit";
// import {newsServices} from '../../services/news.service';
// import {INews} from '../../helpers/types/news';

export interface IControlsState {
  isLoading: boolean;
  isFullListModalShown: boolean;
  isErrorInformingModalShown: boolean;
  errorInformingText: string;
}

const initialState: IControlsState = {
  isLoading: false,
  isFullListModalShown: false,
  isErrorInformingModalShown: false,
  errorInformingText: "myerror",
};

const controlsSlice = createSlice({
  name: "controls",
  initialState,
  reducers: {
    showLoader: (state) => {
      state.isLoading = true;
    },

    hideLoader: (state) => {
      state.isLoading = false;
    },
    showFullListModal: (state) => {
      state.isFullListModalShown = true;
    },
    hideFullListModal: (state) => {
      state.isFullListModalShown = false;
    },
    showErrorInformingModal: (state, action) => {
      state.isErrorInformingModalShown = true;
      state.errorInformingText = action.payload.errorInformingText;
    },
    hideErrorInformingModal: (state) => {
      state.isErrorInformingModalShown = false;
      state.errorInformingText = "";
    },
  },
});

// export const fetchNewsListByQuery = createAsyncThunk(
//   "news/newsListByQuery",
//   async (searchParam: string) => {

//   }
// );
// export const fetchNewsById = createAsyncThunk(
//   "news/newsById",
//   async (searchParam: string) => {
//     const news = await newsServices.getArticleById(searchParam);
//     return { data: news };
//   }
// );

export const { reducer: controlsReducer, actions: controlsActions } =
  controlsSlice;
