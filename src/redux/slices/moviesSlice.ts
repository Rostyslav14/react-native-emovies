import {metadataService} from '../../services/metadata.service';
import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import {IGenre} from '../../helpers/types/shared';

export interface IControlsState {
  genres: IGenre[];
  loading: boolean;
  loaded: boolean;
  errorMessage: string;
}

const initialState: IControlsState = {
  genres: [],
  loading: false,
  loaded: false,
  errorMessage: '',
};

const moviesSlice = createSlice({
  name: 'movies',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(fetchMoviesGenres.pending, state => {
        state.loading = true;
        state.loaded = false;
        state.errorMessage = '';
      })
      .addCase(fetchMoviesGenres.fulfilled, (state, action) => {
        state.loading = false;
        state.loaded = true;
        state.genres = action.payload.genres;
      })
      .addCase(fetchMoviesGenres.rejected, (state, action) => {
        state.loading = false;
        state.loaded = true;
        state.errorMessage = action.payload as string;
      });
  },
});

export const fetchMoviesGenres = createAsyncThunk('news/newsById', async () => {
  const genres = await metadataService.getMoviesGenres();
  return {genres: genres};
});

export const {reducer: moviesReducer, actions: moviesActions} = moviesSlice;
