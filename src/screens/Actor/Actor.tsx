import { Layout } from "../../components/layout/Layout";
import { Loader } from "../../components/shared/Loader";
import { ScrollView } from "react-native";
import React, { useEffect, useState } from "react";
import { FC } from "react";
import { useAppSelector } from "../../redux/store";
import { useNavigation } from "@react-navigation/native";
import { actorsService } from "../../services/actors.service";
import { IActorDetails } from "../../helpers/types/actors";
import { ActorDetailsPreview } from "../../components/pages/Actors/ActorDetailsPreview";
import { Biography } from "../../components/pages/Actors/Biography";
import { IMovie } from "../../helpers/types/movie";
import { MoviesCardList } from "../../components/shared/CardsLists/MoviesCardList";
import { size } from "lodash";
import { TVShowsCardList } from "../../components/shared/CardsLists/TVShowsCardList";
import { ITVShow } from "../../helpers/types/tvShow";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import {
  moviesGenresEncodes,
  tvGenresEncodes,
} from "../../helpers/constants/genreEncodes";

interface IProps {
  route: any;
}
export const Actor: FC<IProps> = (props) => {
  const { route } = props;
  const navigation = useNavigation();
  const id = route.params?.id;
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);
  const [details, setDetails] = useState<IActorDetails>();
  const [moviesWithActor, setMoviesWithActor] = useState<IMovie[]>([]);
  const [tvShowsWithActor, setTvShowsWithActor] = useState<ITVShow[]>([]);
  const connectedControlsActions = useActionCreators(controlsActions);

  useEffect(() => {
    connectedControlsActions.showLoader();
    actorsService
      .getActorDetails(id)
      .then((data) => setDetails(data))
      .catch((err) => console.error(err));
    actorsService
      .getMovieCredits(id)
      .then((data) =>
        setMoviesWithActor(parseGenres(moviesGenresEncodes, data.slice(0, 10)))
      )
      .catch((err) => console.error(err));
    actorsService
      .getTVShowCredits(id)
      .then((data) =>
        setTvShowsWithActor(parseGenres(tvGenresEncodes, data.slice(0, 10)))
      )
      .catch((err) => console.error(err))
      .finally(() => connectedControlsActions.hideLoader());
  }, [id]);

  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
          {!!details?.id && (
            <>
              <ActorDetailsPreview
                imgUrl={details.profile_path}
                birthDate={details.birthday}
                birthPlace={details.place_of_birth}
                knownFor={details.known_for_department}
                name={details.name}
              />
              <Biography biography={details.biography} />
            </>
          )}
          {!!moviesWithActor.length && (
            <MoviesCardList
              cardVariants={{ size: "normal", shape: "square" }}
              listName="Movies"
              itemsList={moviesWithActor}
            />
          )}
          {!!tvShowsWithActor.length && (
            <TVShowsCardList
              cardVariants={{ size: "normal", shape: "square" }}
              listName="TV Shows"
              itemsList={tvShowsWithActor}
            />
          )}
        </ScrollView>
      )}
    </Layout>
  );
};
