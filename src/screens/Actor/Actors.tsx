import React, { useEffect, useState } from "react";
import { FC } from "react";
import { useAppSelector } from "../../redux/store";
import { ScrollView } from "react-native";
import { Layout } from "../../components/layout/Layout";
import { Loader } from "../../components/shared/Loader";
import { IActor } from "../../helpers/types/actors";
import { actorsService } from "../../services/actors.service";
import { ActorsCardList } from "../../components/shared/CardsLists/ActorsCardList";
import { sharedService } from "../../services/shared.service";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { ActorsLongCardList } from "../../components/shared/CardsLists/ActorsLongCardList";
import { GroupArrayItems } from "../../helpers/utils/GroupArrayItems";

interface IProps {}
export const Actors: FC<IProps> = () => {
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);
  const connectedControlsActions = useActionCreators(controlsActions);

  const [popularActors, setPopularActors] = useState<IActor[]>([]);
  const [trendingActors, setTrendingActors] = useState<IActor[][]>([]);

  useEffect(() => {
    connectedControlsActions.showLoader();

    actorsService
      .getActorsList("popular")
      .then((data) => setPopularActors(data))
      .catch((err) => console.error(err));
    sharedService
      .getTrending("person")
      .then((data) => setTrendingActors(GroupArrayItems(data.slice(0, 12), 4)))
      .catch((err) => console.error(err))
      .finally(() => connectedControlsActions.hideLoader());
  }, []);

  
  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
            <ActorsCardList
              cardVariants={{ size: "normal", shape: "rounded" }}
              itemsList={popularActors}
              listName={"Popular"}
            />
            <ActorsLongCardList
              cardVariants={{ size: "big", shape: "rounded" }}
              itemsList={trendingActors}
              listName={"Trending"}
            />
        </ScrollView>
      )}
    </Layout>
  );
};
