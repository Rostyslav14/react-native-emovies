import React, { useState } from "react";
import { FC } from "react";
import { KeyboardAvoidingView } from "react-native";
import { Layout } from "../../components/layout/Layout";
import styled from "styled-components/native";
import { ENScreens } from "../../helpers/constants/routes";
import { PasswordInput } from "../../components/auth/PasswordInput";
import { BottomLine } from "../../components/auth/BottomLine";
import { BasicInput } from "../../components/auth/BasicInput";
import { AuthBtn } from "../../components/auth/AuthBtn";
import { Field, Formik } from "formik";
import * as yup from "yup";
import { authService } from "../../services/auth/auth.service";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { FIREBASE_ERRORS } from "../../firebase/errors";
import { useNavigation } from "@react-navigation/native";
import { AppLogo } from "../../components/auth/AppLogo";

interface Props {}

export const SignIn: FC<Props> = (props) => {
  const signInValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email("Email Shoud have correct format")
      .required("Email is a required input"),
    password: yup
      .string()
      .required("Password is a required input")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
      ),
  });

  const [isAuthLoading, setIsAuthLoading] = useState(false);
  const connectedControlsActions = useActionCreators(controlsActions);
  const navigation  = useNavigation<any>()

  const authErrorHandler = (err: Error) => {
    if (Object.keys(FIREBASE_ERRORS).includes(err.message)) {
      connectedControlsActions.showErrorInformingModal({
        errorInformingText:
          FIREBASE_ERRORS[err.message as keyof typeof FIREBASE_ERRORS],
      });
    } else {
      connectedControlsActions.showErrorInformingModal({
        errorInformingText: "Something went wrong...",
      });
    }
  };

  return (
    <Layout>
      <KeyboardAvoidingView
        behavior="padding"
        style={{ paddingHorizontal: 10 }}
      >
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          validationSchema={signInValidationSchema}
          validateOnChange={false}
          onSubmit={(values) => {
            setIsAuthLoading(true);
            authService
              .signIn(values.email, values.password).then(()=>navigation.navigate(ENScreens.CABINET))
              .catch((err) => authErrorHandler(err))
              .finally(() => setIsAuthLoading(false));
          }}
        >
          {({ handleChange, handleSubmit, values, errors, isValid }) => (
            <>
                    <AppLogo/>

              <Field
                component={BasicInput}
                name="email"
                label="Email"
                leftIconName={"email"}
              />
              <Field
                component={PasswordInput}
                name="password"
                label="Password"
              />
              <AuthBtn isLoading={isAuthLoading} onPress={handleSubmit}>
                Sign In
              </AuthBtn>
            </>
          )}
        </Formik>
        <BottomLine
          description="Don't have a account ?"
          linkText="SIGN UP"
          linkPage={ENScreens.SIGN_UP}
        />
      </KeyboardAvoidingView>
    </Layout>
  );
};
