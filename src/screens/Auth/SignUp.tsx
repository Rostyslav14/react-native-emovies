import React, { useState } from "react";
import { FC } from "react";
import { KeyboardAvoidingView } from "react-native";
import { Layout } from "../../components/layout/Layout";
import styled from "styled-components/native";
import { ENScreens } from "../../helpers/constants/routes";
import { PasswordInput } from "../../components/auth/PasswordInput";
import { Field, Formik } from "formik";
import { BasicInput } from "../../components/auth/BasicInput";
import * as yup from "yup";
import { BottomLine } from "../../components/auth/BottomLine";
import { AuthBtn } from "../../components/auth/AuthBtn";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { FIREBASE_ERRORS } from "../../firebase/errors";
import { authService } from "../../services/auth/auth.service";
import { AppLogo } from "../../components/auth/AppLogo";

interface Props {}

export const SignUp: FC<Props> = (props) => {
  const signUpValidationSchema = yup.object().shape({
    email: yup
      .string()
      .email("Email Shoud have correct format")
      .required("Email is a required input"),
    password: yup
      .string()
      .required("Password is a required input")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})/,
        "Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and One Special Case Character"
      ),
    confirmPassword: yup
      .string()
      .required("Confirm Password is required")
      .oneOf([yup.ref("password")], "Passwords does not match"),
  });
  const [isAuthLoading, setIsAuthLoading] = useState(false);
  const connectedControlsActions = useActionCreators(controlsActions);

  const authErrorHandler = (err: Error) => {
    if (Object.keys(FIREBASE_ERRORS).includes(err.message)) {
      connectedControlsActions.showErrorInformingModal({
        errorInformingText:
          FIREBASE_ERRORS[err.message as keyof typeof FIREBASE_ERRORS],
      });
    } else {
      connectedControlsActions.showErrorInformingModal({
        errorInformingText: "Something went wrong...",
      });
    }
  };

  return (
    <Layout>
      <KeyboardAvoidingView
        behavior="padding"
        style={{ paddingHorizontal: 10 }}
      >
        <AppLogo/>

        <Formik
          initialValues={{
            username: "",
            email: "",
            password: "",
            confirmPassword: "",
          }}
          validationSchema={signUpValidationSchema}
          validateOnChange={false}
          onSubmit={(values) => {
            setIsAuthLoading(true);
            authService
              .signUp(values)
              .catch((err) => authErrorHandler(err))
              .finally(() => setIsAuthLoading(false));
          }}
        >
          {({ handleChange, handleSubmit, values, errors, isValid }) => (
            <>
              <Field
                component={BasicInput}
                name="username"
                leftIconName="account"
                label="Username"
              />
              <Field
                component={BasicInput}
                name="email"
                leftIconName="email"
                label="Email"
              />
              <Field
                component={PasswordInput}
                name="password"
                label="Password"
              />
              <Field
                component={PasswordInput}
                name="confirmPassword"
                label="Confirm Password"
              />

              <AuthBtn isLoading={isAuthLoading} onPress={handleSubmit}>
                Sign Up
              </AuthBtn>
            </>
          )}
        </Formik>
        <BottomLine
          linkText="SIGN IN"
          linkPage={ENScreens.SIGN_IN}
          description="Already have an account ?"
        />
      </KeyboardAvoidingView>
    </Layout>
  );
};
