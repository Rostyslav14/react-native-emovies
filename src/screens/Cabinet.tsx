import React from "react";
import { FC } from "react";
import { View } from "react-native";
import { Layout } from "../components/layout/Layout";
import { MenuItem } from "../components/pages/Cabinet/MenuItem";
import Feather from "react-native-vector-icons/Feather";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import { styleVariables } from "../styles/variables";
import { HeaderBlock } from "../components/pages/Cabinet/HeaderBlock";
import { useIsAuthStatus } from "../helpers/hooks/useIsAuthStatus";
import { useNavigation } from "@react-navigation/native";
import { ENScreenGroups, ENScreens } from "../helpers/constants/routes";
import { BasicButton } from "../components/shared/BasicButton";
import { authService } from "../services/auth/auth.service";
import { auth } from "../firebase/firebase";

export const Cabinet: FC = () => {
  const isLoggedIn = useIsAuthStatus();
  const navigation = useNavigation<any>();
  const onSignInPress = () => navigation.navigate(ENScreenGroups.AUTH_GROUP);
  const onSignOutPress = () => authService.logOut();
  return (
    <Layout>
      <View>
        <HeaderBlock header={isLoggedIn ? auth.name : 'Your account'}>
          {!isLoggedIn && (
            <BasicButton onPress={() => onSignInPress()} text={"Sign In"} />
          )}
        </HeaderBlock>
        <MenuItem
          header={"Favorite"}
          additionalText={"Your favorite Movies & Tv Shows"}
        >
          <MaterialIcons
            color={styleVariables.colors.greenBright}
            size={23}
            name="favorite-outline"
          />
        </MenuItem>
        <MenuItem
          header={"WatchList"}
          additionalText={"Movies and TvShows Added to watchlist"}
        >
          <Feather
            color={styleVariables.colors.greenBright}
            size={23}
            name="bookmark"
          />
        </MenuItem>
        <MenuItem header={"Ratings"} additionalText={"Rated Movies & Tv Shows"}>
          <MaterialIcons
            color={styleVariables.colors.greenBright}
            size={23}
            name="star-outline"
          />
        </MenuItem>
        {isLoggedIn && <BasicButton onPress={() => onSignOutPress()} text={"Sign Out"} />}

      </View>
    </Layout>
  );
};
