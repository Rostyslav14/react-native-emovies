import React, { useState } from "react";
import { FC } from "react";
import {
  Modal,
  View,
  Pressable,
  Text,
  StyleSheet,
  Dimensions,
} from "react-native";
import { styleVariables } from "../styles/variables";
import { useAppSelector } from "../redux/store";
import { useActionCreators } from "../helpers/hooks/useActionCreator";
import { controlsActions } from "../redux/slices/controlsSlice";

export const ErrorInformingModal: FC = (props) => {
  const errorInformingText = useAppSelector(
    (store) => store.controlsSlicer.errorInformingText
  );
  const isErrorInformingModalShown = useAppSelector(
    (store) => store.controlsSlicer.isErrorInformingModalShown
  );

  const connectedControlsActions = useActionCreators(controlsActions);
  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={isErrorInformingModalShown}
      onRequestClose={() => {
        connectedControlsActions.hideErrorInformingModal();
      }}
    >
      {isErrorInformingModalShown && (
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>{errorInformingText}</Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => connectedControlsActions.hideErrorInformingModal()}
            >
              <Text style={styles.textStyle}>Ok</Text>
            </Pressable>
          </View>
        </View>
      )}
    </Modal>
  );
};
const styles = StyleSheet.create({
  centeredView: {
    zIndex: 100,
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(11,21,22,.2)",
  },
  modalView: {
    width: Dimensions.get("screen").width - 60,
    minHeight: 120,
    margin: 20,
    borderRadius: 4,
    paddingHorizontal: 15,
    paddingVertical: 15,
    alignItems: "center",
    backgroundColor: styleVariables.colors.greenMain,
    shadowColor: "#000",
    justifyContent: "space-between",
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,

    elevation: 24,
  },
  button: {
    borderRadius: 20,
    padding: 10,
  },

  buttonClose: {
    alignSelf: "flex-end",
    backgroundColor: styleVariables.colors.greenMain,
  },
  textStyle: {
    color: styleVariables.colors.greenBright,
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    color: styleVariables.colors.whiteMain,
    marginBottom: 15,
    textAlign: "left",
    fontSize: 15,
  },
});
