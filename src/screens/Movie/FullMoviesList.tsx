import React, { FC, useEffect, useState } from "react";
import { Layout } from "../../components/layout/Layout";
import { ITVShow } from "../../helpers/types/tvShow";
import { IMovie } from "../../helpers/types/movie";
import { IActor } from "../../helpers/types/actors";
import { View, Text } from "react-native";
import { moviesGenresEncodes } from "../../helpers/constants/genreEncodes";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import { movieService } from "../../services/movie.service";
import { useAppSelector } from "../../redux/store";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { MOVIES_LIST_REQUEST_VARIANTS } from "../../helpers/constants/requestConstants";
import { Loader } from "../../components/shared/Loader";
import { MovieFullWidthCardsList } from "../../components/shared/CardsLists/FullWidthCardsLists/MovieFullWidthCardsList";

interface IProps {
  route: any;
  navigation: any;
}
export const FullMoviesList: FC<IProps> = (props) => {
  const [moviesList, setMoviesList] = useState<IMovie[]>([]);
  const connectedControlsActions = useActionCreators(controlsActions);
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);
  const [lastFetchPage, setLastFetchPage] = useState(1);

  const fetchCategory = props.route.params?.listCategory;
  console.log(props);

  useEffect(() => {
    if (MOVIES_LIST_REQUEST_VARIANTS.includes(fetchCategory)) {
      connectedControlsActions.showLoader();
      movieService
        .getList(fetchCategory, lastFetchPage)
        .then((data) =>
          setMoviesList((prev) => [
            ...prev,
            ...parseGenres(moviesGenresEncodes, data),
          ])
        )
        .catch((err) => console.error(err))
        .finally(() => connectedControlsActions.hideLoader());
    }
  }, [lastFetchPage]);

  const onEndReached = () => setLastFetchPage((prev) => prev + 1);
  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <MovieFullWidthCardsList
          onEndReached={onEndReached}
          itemsList={moviesList}
        />
      )}
    </Layout>
  );
};
