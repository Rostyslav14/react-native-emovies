import { useNavigation } from "@react-navigation/native";
import { FC, useEffect, useState } from "react";
import { ScrollView } from "react-native";
import { movieService } from "../../services/movie.service";
import { IMovie, IMovieDetails } from "../../helpers/types/movie";
import { ICastMember, IVideo } from "../../helpers/types/shared";
import React from "react";
import { styleVariables } from "../../styles/variables";
import { DetailsPreview } from "../../components/shared/DetailsPreview";
import { useAppSelector } from "../../redux/store";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { Loader } from "../../components/shared/Loader";
import { MoviesCardList } from "../../components/shared/CardsLists/MoviesCardList";
import { CollectionsPreview } from "../../components/pages/Movie/CollectionsPreview";
import { GradientPoster } from "../../components/shared/GradientPoster";
import { VideoPreviewList } from "../../components/shared/VideoPreviewList";
import config from "../../config";
import { MovieInformation } from "../../components/pages/Movie/MovieInformation";
import { Layout } from "../../components/layout/Layout";
import { RatingLine } from "../../components/shared/RatingLine";
import { CrewCardsList } from "../../components/shared/CardsLists/CrewCardsList";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import { moviesGenresEncodes } from "../../helpers/constants/genreEncodes";
import { HeaderWithIcons } from "../../components/layout/Headers/HeaderWithIcons";
import { cabinetActions } from "../../redux/slices/cabinetSlice";
import { cabinetSelectors } from "../../redux/selectors/cabinetSelectors";
import { ENScreens } from "../../helpers/constants/routes";
import { HeaderIoniIcon } from "../../components/shared/HeaderIoniIcon";
import { DecodeISO6391 } from "../../helpers/utils/DecodeISO639-1Lang";

interface IProps {
  route: any;
}

export const Movie: FC<IProps> = (props) => {
  const { route } = props;
  const navigation = useNavigation<any>();
  const id = route.params?.id as number;
  const connectedControlsActions = useActionCreators(controlsActions);
  const connectedCabinetActions = useActionCreators(cabinetActions);
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);
  const movies = useAppSelector((store) => store.cabinetSlicer.favorites);

  const isFavorite = useAppSelector((state) =>
    cabinetSelectors.movies.isFavorite(state, id)
  );
  const isInWatchList = useAppSelector((state) =>
    cabinetSelectors.movies.isInWatchList(state, id)
  );
  const isRated = useAppSelector((state) =>
    cabinetSelectors.movies.isRated(state, id)
  );

  const [details, setDetails] = useState<IMovieDetails>();
  const [recomendedMovies, setRecomendedMovies] = useState<IMovie[]>([]);
  const [similarMovies, setSimilarMovies] = useState<IMovie[]>([]);
  const [videoItems, setVideoItems] = useState<IVideo[]>([]);
  const [cast, setCast] = useState<ICastMember[]>([]);

  useEffect(() => {
    if (id) {
      connectedControlsActions.showLoader();
      movieService
        .getListById(id, "similar")
        .then((data) =>
          setSimilarMovies(parseGenres(moviesGenresEncodes, data))
        )
        .catch((err) => console.error(err));
      movieService
        .getCastAndCrewById(id)
        .then((data) => setCast(data.cast))
        .catch((err) => console.error(err));

      movieService
        .getListById(id, "recommendations")
        .then((data) =>
          setRecomendedMovies(parseGenres(moviesGenresEncodes, data))
        )
        .catch((err) => console.error(err));
      movieService
        .getVideosById(id)
        .then((data) => setVideoItems(data))
        .catch((err) => console.error(err));
      movieService
        .getDetailsById(id)
        .then((data) => setDetails(data))
        .catch((err) => console.error(err))
        .finally(() => connectedControlsActions.hideLoader());
    }
  }, [id]);

  const onFavoriteClick = () => {
    if (isFavorite) {
      connectedCabinetActions.removeFavoriteMovie({ movieId: id });
    } else {
      connectedCabinetActions.addFavoriteMovie({
        movie: details as IMovieDetails,
      });
    }
  };
  const onRatingClick = () => {
    navigation.navigate(ENScreens.RATE_MODAL, {
      itemDetails: details,
    });
  };

  const onWatchListClick = () => {
    if (isInWatchList) {
      connectedCabinetActions.removeWatchListMovie({
        movieId: id,
      });
    } else {
      connectedCabinetActions.addWatchListMovie({
        movie: details as IMovieDetails,
      });
    }
  };

  return (
    <>
      <HeaderWithIcons navigation={navigation} title={route.params?.pageName}>
        <HeaderIoniIcon
          isActive={isFavorite}
          onPress={onFavoriteClick}
          unActiveName="md-heart-outline"
          activeName="md-heart-sharp"
        />
        <HeaderIoniIcon
          onPress={onRatingClick}
          isActive={isRated}
          unActiveName="ios-star-outline"
          activeName="ios-star"
        />
        <HeaderIoniIcon
          onPress={onWatchListClick}
          isActive={isInWatchList}
          unActiveName="ios-bookmark-outline"
          activeName="ios-bookmark"
        />
      </HeaderWithIcons>
      <Layout>
        {isLoading ? (
          <Loader />
        ) : (
          <ScrollView overScrollMode="never">
            {!!details?.id && (
              <>
                <GradientPoster
                  ImgUrl={config.apiImgUrl + details.backdrop_path}
                  locations={[0, 0.9]}
                  colors={["transparent", styleVariables.colors.greenMain]}
                />
                <DetailsPreview
                  description={details.overview}
                  genres={details.genres}
                  title={details.title}
                  posterImgUrl={details.poster_path}
                >
                  <RatingLine
                    rating={{
                      count: details.vote_count,
                      average: details.vote_average,
                    }}
                  />
                </DetailsPreview>
                {!!details.belongs_to_collection?.id! && (
                  <CollectionsPreview
                    collection={details.belongs_to_collection}
                    genres={details.genres}
                  />
                )}
                <CrewCardsList crewList={cast} />
                <MovieInformation
                  language={DecodeISO6391(details.original_language)}
                  releaseDate={details.release_date}
                  budget={details.budget}
                  revenue={details.revenue}
                  productionCompanies={details.production_companies}
                />
              </>
            )}
            <VideoPreviewList previewVideoItems={videoItems} />
            {!!recomendedMovies.length && (
              <MoviesCardList
                listCategory="recommendations"
                cardVariants={{ size: "normal", shape: "square" }}
                itemsList={recomendedMovies}
                listName="Recomended"
              />
            )}
            {!!similarMovies.length && (
              <MoviesCardList
                listCategory="similar"
                cardVariants={{ size: "normal", shape: "square" }}
                itemsList={similarMovies}
                listName="Similar"
              />
            )}
          </ScrollView>
        )}
      </Layout>
    </>
  );
};
