import { FC, useEffect, useState } from "react";
import { ScrollView } from "react-native";
import React from "react";
import { IMovieCollection } from "../../helpers/types/movie";
import { movieService } from "../../services/movie.service";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { useNavigation } from "@react-navigation/native";
import { useAppSelector } from "../../redux/store";
import { Loader } from "../../components/shared/Loader";
import { Layout } from "../../components/layout/Layout";
import { GradientPoster } from "../../components/shared/GradientPoster";
import { styleVariables } from "../../styles/variables";
import config from "../../config";
import { CollectionDescription } from "../../components/pages/Movie/CollectionDescription";
import { CollectionCardsList } from "../../components/pages/Movie/CollectionCardsList";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import { moviesGenresEncodes } from "../../helpers/constants/genreEncodes";

interface IProps {
  route: any;
}
export const MovieCollection: FC<IProps> = (props) => {
  const { route } = props;
  const navigation = useNavigation();
  const id = route.params?.id;
  const connectedControlsActions = useActionCreators(controlsActions);
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);

  const [details, setDetails] = useState<IMovieCollection>();

  useEffect(() => {
    connectedControlsActions.showLoader();
    movieService
      .getCollectionById(id)
      .then((data) => {
        data.parts = parseGenres(moviesGenresEncodes, data.parts);
        return data;
      })
      .then((data) => setDetails(data))
      .catch((err) => console.error(err))
      .finally(() => connectedControlsActions.hideLoader());
  }, []);

  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
          {details && (
            <>
              <GradientPoster
                locations={[0, 0.9]}
                colors={["transparent", styleVariables.colors.greenMain]}
                ImgUrl={config.apiImgUrl + details?.backdrop_path}
                height={180}
              />
              <CollectionDescription
                title={details.name}
                imageUrl={details.poster_path}
                text={details.overview}
              />
              <CollectionCardsList crewList={details.parts} />
            </>
          )}
        </ScrollView>
      )}
    </Layout>
  );
};
