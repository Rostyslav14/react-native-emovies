import { FC, useEffect, useState } from "react";
import { ScrollView, View } from "react-native";
import React from "react";
import { IMovie } from "../../helpers/types/movie";
import { movieService } from "../../services/movie.service";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { sharedService } from "../../services/shared.service";
import { GroupArrayItems } from "../../helpers/utils/GroupArrayItems";
import { NavigationProp } from "@react-navigation/native";
import { useAppSelector } from "../../redux/store";
import { Loader } from "../../components/shared/Loader";
import { Layout } from "../../components/layout/Layout";
import { MoviesCardList } from "../../components/shared/CardsLists/MoviesCardList";
import { MoviesLongCardList } from "../../components/shared/CardsLists/MovieLongCardsList";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import { moviesGenresEncodes } from "../../helpers/constants/genreEncodes";

interface IProps {
  navigation?: NavigationProp<any, any>;
}
export const Movies: FC<IProps> = (props) => {
  const [popularMovies, setPopularMovies] = useState<IMovie[]>([]);
  const [nowPlayingMovies, setNowPlayingMovies] = useState<IMovie[]>([]);
  const [trendingMovies, setTrendingMovies] = useState<IMovie[]>([]);
  const [upcomingMovies, setUpcomingMovies] = useState<IMovie[]>([]);
  const [topRatedMovies, setTopRatedMovies] = useState<IMovie[][]>([]);
  const connectedControlsActions = useActionCreators(controlsActions);
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);

  useEffect(() => {
    connectedControlsActions.showLoader();

    movieService
      .getList("popular")
      .then((data) => setPopularMovies(parseGenres(moviesGenresEncodes, data)))
      .catch((err) => console.error(err));
    movieService
      .getList("now_playing")
      .then((data) =>
        setNowPlayingMovies(parseGenres(moviesGenresEncodes, data))
      )
      .catch((err) => console.error(err));
    movieService
      .getList("top_rated")
      .then((data) =>
        setTopRatedMovies(
          GroupArrayItems(parseGenres(moviesGenresEncodes, data), 4)
        )
      )
      .catch((err) => console.error(err));
    movieService
      .getList("upcoming")
      .then((data) => setUpcomingMovies(parseGenres(moviesGenresEncodes, data)))
      .catch((err) => console.error(err));

    sharedService
      .getTrending("movie")
      .then((data) => setTrendingMovies(parseGenres(moviesGenresEncodes, data)))
      .catch((err) => console.error(err))
      .finally(() => connectedControlsActions.hideLoader());
  }, []);
  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
          <View>
            <MoviesCardList
              listCategory={"popular"}
              cardVariants={{ size: "normal", shape: "square" }}
              listName={"Popular"}
              itemsList={popularMovies}
            />
            <MoviesCardList
              listCategory={"now_playing"}
              cardVariants={{ size: "big", shape: "square" }}
              listName={"Playing In Theaters"}
              itemsList={nowPlayingMovies}
            />
            <MoviesCardList
              listCategory={"trending"}
              cardVariants={{ size: "normal", shape: "square" }}
              listName={"Trending"}
              itemsList={trendingMovies}
            />
            <MoviesLongCardList
              listCategory="top_rated"
              cardVariants={{ size: "normal", shape: "square" }}
              listName={"Top Rated"}
              itemsList={topRatedMovies}
            />
            <MoviesCardList
              listCategory={"upcoming"}
              cardVariants={{ size: "normal", shape: "square" }}
              listName={"Upcoming"}
              itemsList={upcomingMovies}
            />
          </View>
        </ScrollView>
      )}
    </Layout>
  );
};
