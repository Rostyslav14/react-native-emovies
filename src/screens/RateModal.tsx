import React, { useState } from "react";
import { FC } from "react";
import { ImageBackground, ActivityIndicator } from "react-native";
import styled from "styled-components/native";
import config from "../config";
import { LAYOUT } from "../helpers/constants/layout";
import { RatingWithReviews } from "../components/modals/RateModal/RatingWithReviews";
import { BasicButton } from "../components/shared/BasicButton";
import { MovieCover } from "../components/modals/RateModal/MovieCover";
import { useAppSelector } from "../redux/store";
import { movieService } from "../services/movie.service";
import { controlsActions } from "../redux/slices/controlsSlice";
import { useActionCreators } from "../helpers/hooks/useActionCreator";
import { styleVariables } from "../styles/variables";
import { cabinetActions } from "../redux/slices/cabinetSlice";
import { cabinetSelectors } from "../redux/selectors/cabinetSelectors";

interface IProps {
  route: any;
}
const Container = styled.View`
  width: ${LAYOUT.window.width}px;
  height: ${LAYOUT.window.height}px;
`;

const ContentWrapper = styled.View`
  background-color: rgba(0, 0, 0, 0.7);
  width: ${LAYOUT.window.width}px;
  height: ${LAYOUT.window.height}px;
`;
const BtnsMargin = styled.View`
  height: 10px;
`;
export const RateModal: FC<IProps> = (props) => {
  const itemDetails = props.route.params?.itemDetails;
  const connectedControlsActions = useActionCreators(controlsActions);
  const connectedCabinetActions = useActionCreators(cabinetActions);

  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);

  const userRating = useAppSelector((state) =>
    cabinetSelectors.movies.findUserMovieRating(state, itemDetails.id as number)
  );
  const [rateValue, setRateValue] = useState(0);

  const onSetRating = (rate: number) => setRateValue(rate);
  const onPostRating = () => {
    connectedControlsActions.showLoader();
    connectedCabinetActions.addRatedMovie({
      movie: itemDetails,
      userRating: rateValue,
    });
    movieService
      .rateMovie(itemDetails?.id, rateValue)
      .then(() => connectedControlsActions.hideLoader());
  };
  const onDeleteRating = () => {
    connectedControlsActions.showLoader();
    connectedCabinetActions.removeRatedMovie({ movieId: itemDetails.id });
    setRateValue(0);
    movieService
      .deleteMovieRating(itemDetails?.id)
      .then(() => connectedControlsActions.hideLoader());
  };

  return (
    <Container>
      <ImageBackground
        style={{ flex: 1, backgroundColor: "black", marginBottom: 75 }}
        source={{ uri: config.apiImgUrl + itemDetails.poster_path }}
        blurRadius={8}
        resizeMode="contain"
      >
        {isLoading ? (
          <ActivityIndicator
            style={{ alignSelf: "center", marginTop: "80%" }}
            {...props}
            color={styleVariables.colors.greenBright}
            size={"large"}
          />
        ) : (
          <ContentWrapper>
            <MovieCover imageSrc={itemDetails.poster_path} />
            <RatingWithReviews
              rating={userRating || rateValue}
              onFinishRating={(rating: number) => onSetRating(rating)}
            >
              <BasicButton
                isDisabled={rateValue === 0}
                text={"Rate"}
                onPress={() => onPostRating()}
              />
              <BtnsMargin />
              {userRating && (
                <BasicButton
                  isDisabled={false}
                  mainColor="red"
                  text={"Delete Rating"}
                  onPress={() => onDeleteRating()}
                />
              )}
            </RatingWithReviews>
          </ContentWrapper>
        )}
      </ImageBackground>
    </Container>
  );
};
