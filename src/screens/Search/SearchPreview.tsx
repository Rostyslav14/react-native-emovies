import React, { FC, useEffect, useState } from "react";
import { ScrollView } from "react-native";
import { Loader } from "../../components/shared/Loader";
import { useAppSelector } from "../../redux/store";
import { sharedService } from "../../services/shared.service";
import { Trendings } from "../../components/pages/Search/Trendings";
import { IMovie } from "../../helpers/types/movie";
import { IActor } from "../../helpers/types/actors";
import { Layout } from "../../components/layout/Layout";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { ITVShow } from "../../helpers/types/tvShow";

interface IProps {}
export const SearchPreview: FC<IProps> = (props) => {
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);
  const [trending, setTrending] = useState<Array<IMovie | ITVShow | IActor>>();
  const connectedControlsActions = useActionCreators(controlsActions);

  useEffect(() => {
    connectedControlsActions.showLoader()
    sharedService
      .getTrending("all", "day")
      .then((data) => setTrending(data.slice(0,8)))
      .catch((err) => console.error(err))
      .finally(()=>connectedControlsActions.hideLoader())
  }, []);
  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
          {!!trending && <Trendings trendings={trending} />}
        </ScrollView>
      )}
    </Layout>
  );
};
