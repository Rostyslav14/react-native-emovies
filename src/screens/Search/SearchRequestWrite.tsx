import { FC, useEffect, useState } from "react";
import { useAppSelector } from "../../redux/store";
import React from "react";
import { Layout } from "../../components/layout/Layout";
import { Loader } from "../../components/shared/Loader";
import { debounce } from "lodash";
import { SearchInputLine } from "../../components/shared/SearchInputLine";
import { SearchRequestsList } from "../../components/pages/Search/SearchRequestsList";
import { searchService } from "../../services/search.service";

interface IProps {}
export const SearchRequestWrite: FC<IProps> = (props) => {
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);
  const [searchRequest,setSearchRequest] = useState<string[]>([])
  useEffect(()=>{
    searchService.getSearchList('a').then(data=>setSearchRequest(data.map(el=>el.title || el.name)))
  },[])
  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <SearchInputLine value="aa" />
          {/* <Text
            style={{
              flex: 1,
              textAlignVertical: "center",
              textAlign: "center",
              fontSize: 18,
              color: "white",
              fontWeight: "600",
            }}
          >
            Search for movies, tv shows, celebreties
          </Text> */}
          <SearchRequestsList requests={searchRequest} />

        </>
      )}
    </Layout>
  );
};
