import { useNavigation } from "@react-navigation/native";
import { FC, useEffect, useState } from "react";
import { ScrollView } from "react-native";
import React from "react";
import { styleVariables } from "../../styles/variables";
import { DetailsPreview } from "../../components/shared/DetailsPreview";
import { useAppSelector } from "../../redux/store";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { Loader } from "../../components/shared/Loader";
import { GradientPoster } from "../../components/shared/GradientPoster";
import { VideoPreviewList } from "../../components/shared/VideoPreviewList";
import config from "../../config";
import { Layout } from "../../components/layout/Layout";
import { RatingLine } from "../../components/shared/RatingLine";
import { ITVShow, ITVShowDetails } from "../../helpers/types/tvShow";
import { tvShowsService } from "../../services/tvShow.service";
import { ICastMember, ISeason, IVideo } from "../../helpers/types/shared";
import { TVShowsCardList } from "../../components/shared/CardsLists/TVShowsCardList";
import { CrewCardsList } from "../../components/shared/CardsLists/CrewCardsList";
import { SeasonCardsList } from "../../components/pages/TVShows/SeasonsCardList";
import { tvGenresEncodes } from "../../helpers/constants/genreEncodes";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import { TVInformation } from "../../components/pages/TVShows/TVInformation";
interface IProps {
  route: any;
}

export const TVShow: FC<IProps> = (props) => {
  const { route } = props;
  const navigation = useNavigation();
  const id = route.params?.id;
  const connectedControlsActions = useActionCreators(controlsActions);
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);

  const [details, setDetails] = useState<ITVShowDetails>();
  const [recomendedTVShows, setRecomendedTVShows] = useState<ITVShow[]>([]);
  const [similarTVShows, setSimilarTVShows] = useState<ITVShow[]>([]);
  const [videoItems, setVideoItems] = useState<IVideo[]>([]);
  const [cast, setCast] = useState<ICastMember[]>([]);
  const [seasons, setSeasons] = useState<ISeason[]>([]);
  useEffect(() => {
    if (id) {
      connectedControlsActions.showLoader();
      tvShowsService
        .getCastAndCrew(id)
        .then((data) => setCast(data.cast))
        .catch((err) => console.error(err));
      tvShowsService
        .getDetails(id)
        .then((data) => setDetails(data))
        .catch((err) => console.error(err));

      tvShowsService
        .getListById(id, "similar")
        .then((data) => setSimilarTVShows(parseGenres(tvGenresEncodes,data)))
        .catch((err) => console.error(err));

      tvShowsService
        .getListById(id, "recommendations")
        .then((data) => setRecomendedTVShows(parseGenres(tvGenresEncodes,data)))
        .catch((err) => console.error(err));
      tvShowsService
        .getSeasons(id)
        .then((data) => setSeasons(data.filter((el) => !!el.poster_path)))
        .catch((err) => console.error(err));
      tvShowsService
        .getVideos(id)
        .then((data) => setVideoItems(data))
        .catch((err) => console.error(err))
        .finally(() => connectedControlsActions.hideLoader());
    }
  }, [id]);

  const onRate = () => {
    return "something";
  };
  // useEffect(()=>{
  //   navigation.setParams({title:details?.name})
  // },[id])

  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
          {!!details?.id && (
            <>
              <GradientPoster
                ImgUrl={config.apiImgUrl + details.backdrop_path}
                locations={[0, 0.9]}
                colors={["transparent", styleVariables.colors.greenMain]}
              />
              <DetailsPreview
                description={details.overview}
                genres={details.genres}
                title={details.name}
                posterImgUrl={details.poster_path}
              >
                <RatingLine
                  onRate={onRate}
                  rating={{
                    count: details.vote_count,
                    average: details.vote_average,
                  }}
                />
              </DetailsPreview>
            </>
          )}
          {!!seasons.length && (
            <SeasonCardsList
              tvShowName={details?.name!}
              tvShowId={id}
              seasons={seasons}
            />
          )}
          <CrewCardsList crewList={cast} />
          {details && <TVInformation networks={details.networks} originCountry={details.origin_country} language={details.original_language} airDate={details.first_air_date} createdBy={details.created_by} />}

          <VideoPreviewList previewVideoItems={videoItems} />

          {!!recomendedTVShows.length && (
            <TVShowsCardList
              cardVariants={{ size: "normal", shape: "square" }}
              itemsList={recomendedTVShows}
              listName="Recomended"
            />
          )}

          {!!similarTVShows.length && (
            <TVShowsCardList
              cardVariants={{ size: "normal", shape: "square" }}
              itemsList={similarTVShows}
              listName="Similar"
            />
          )}
        </ScrollView>
      )}
    </Layout>
  );
};
