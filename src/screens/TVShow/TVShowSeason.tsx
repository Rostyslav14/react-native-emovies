import { FC, useEffect, useMemo, useState } from "react";
import { ScrollView } from "react-native";
import React from "react";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { useNavigation } from "@react-navigation/native";
import { useAppSelector } from "../../redux/store";
import { Loader } from "../../components/shared/Loader";
import { Layout } from "../../components/layout/Layout";
import {
  IEpisodeGroup,
  ISesonDetails,
  ITVShowEpisode,
} from "../../helpers/types/tvShow";
import { tvShowsService } from "../../services/tvShow.service";
import { HeaderBlock } from "../../components/pages/TVShows/SeasonPage/HeaderBlock";
import { DisabledRatingLine } from "../../components/shared/DisabledRatingLine";
import { EpisodesCardsList } from "../../components/pages/TVShows/SeasonPage/EpisodesCardList";

function countSeasonVoteData(episodes: ITVShowEpisode[]) {
  let voteCount = [];
  let voteAverage = [];
  for (let el of episodes) {
    voteCount.push(el.vote_count);
    voteAverage.push(el.vote_average);
  }
  const averageCount = episodes.reduce((acc, el) => {
    acc += el.vote_count;
    return acc;
  }, 0);
  const averageRating = episodes.reduce((acc, el) => {
      
    acc += el.vote_average;
    return acc;
  }, 0)/episodes.length;
  return { count:averageCount, rating:+averageRating.toFixed(1) };
}

interface IProps {
  route: any;
}
export const TVShowSeason: FC<IProps> = (props) => {
  const { route } = props;

  const { tvId, seasonNumber, tvShowName } = route.params;
  const connectedControlsActions = useActionCreators(controlsActions);
  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);

  const [details, setDetails] = useState<ISesonDetails>();
  const seasonAverageRate = useMemo(() => {
    if (!!details?.episodes.length)
      return countSeasonVoteData(details.episodes);
  }, [details]);

  useEffect(() => {
    // connectedControlsActions.showLoader();
    tvShowsService
      .getSeasonDetails(tvId, seasonNumber)
      .then((data) => setDetails(data))
      .catch((err) => console.error(err));
  }, [tvId, seasonNumber]);
  console.log(details)

  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
          {details && (
            <>
              <HeaderBlock
                title={tvShowName}
                airingDate={details.air_date}
                seasonNumber={details.season_number}
                imgUrl={details.poster_path}
                overview={details.overview}
              >
                <DisabledRatingLine starSize={10}  voteAverage={seasonAverageRate?.rating || 0} voteCount={seasonAverageRate?.count || 0} />
              </HeaderBlock>
              <EpisodesCardsList showBackdropImg={details.poster_path} episodes={details.episodes}/>
            </>
          )}
        </ScrollView>
      )}
    </Layout>
  );
};
