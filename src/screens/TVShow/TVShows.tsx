import React, { useEffect, useState } from "react";
import { FC } from "react";
import { ScrollView } from "react-native";
import { Layout } from "../../components/layout/Layout";
import { useAppSelector } from "../../redux/store";
import { controlsActions } from "../../redux/slices/controlsSlice";
import { useActionCreators } from "../../helpers/hooks/useActionCreator";
import { ITVShow } from "../../helpers/types/tvShow";
import { tvShowsService } from "../../services/tvShow.service";
import { sharedService } from "../../services/shared.service";
import { TVShowsCardList } from "../../components/shared/CardsLists/TVShowsCardList";
import { Loader } from "../../components/shared/Loader";
import { GroupArrayItems } from "../../helpers/utils/GroupArrayItems";
import { TVShowsLongCardList } from "../../components/shared/CardsLists/TVShowsLongCardList";
import { parseGenres } from "../../helpers/utils/ParseGenres";
import { tvGenresEncodes } from "../../helpers/constants/genreEncodes";

export const TVShows: FC = () => {
  const [airingTodayShows, setAiringTodayShows] = useState<ITVShow[]>([]);
  const [trendingShows, setTrendingShows] = useState<ITVShow[]>([]);
  const [popularShows, setPopularShows] = useState<ITVShow[]>([]);
  const [topRatedShows, setTopRatedShows] = useState<ITVShow[][]>([]);

  const connectedControlsActions = useActionCreators(controlsActions);

  const isLoading = useAppSelector((store) => store.controlsSlicer.isLoading);

  useEffect(() => {
    connectedControlsActions.showLoader();
    tvShowsService
      .getList("airing_today")
      .then((data) => setAiringTodayShows(parseGenres(tvGenresEncodes,data)))
      .catch((err) => console.error(err));
    tvShowsService
      .getList("popular")
      .then((data) => setPopularShows(parseGenres(tvGenresEncodes,data)))
      .catch((err) => console.error(err));
    tvShowsService
      .getList("top_rated")
      .then((data) => setTopRatedShows(GroupArrayItems(parseGenres(tvGenresEncodes,data), 4)))
      .catch((err) => console.error(err));

    sharedService
      .getTrending("tv")
      .then((data) => setTrendingShows(parseGenres(tvGenresEncodes,data)))
      .catch((err) => console.error(err))
      .finally(() => connectedControlsActions.hideLoader());
  }, []);

  return (
    <Layout>
      {isLoading ? (
        <Loader />
      ) : (
        <ScrollView overScrollMode="never">
            <TVShowsCardList
              cardVariants={{ shape: "square", size: "big" }}
              listName={"Aiting Today"}
              itemsList={airingTodayShows}
            />
            <TVShowsCardList
              cardVariants={{ shape: "square", size: "normal" }}
              listName={"Trending"}
              itemsList={trendingShows}
            />
            <TVShowsLongCardList
              cardVariants={{ shape: "square", size: "normal" }}
              listName="Top Rated"
              itemsList={topRatedShows}
            />
            <TVShowsCardList
              cardVariants={{ shape: "square", size: "normal" }}
              listName={"Popular"}
              itemsList={popularShows}
            />
        </ScrollView>
      )}
    </Layout>
  );
};
