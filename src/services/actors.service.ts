import { ITVShow } from "./../helpers/types/tvShow";
import { IActor, IActorDetails } from "../helpers/types/actors";
import config from "../config";
import { ACTORS_LIST_REQUEST_VARIANTS } from "../helpers/constants/requestConstants";
import { IMovie } from "../helpers/types/movie";

export const actorsService = {
  getActorsList,
  getActorDetails,
  getMovieCredits,
  getTVShowCredits,
};

function getActorsList(
  category: typeof ACTORS_LIST_REQUEST_VARIANTS[number]
): Promise<IActor[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}person/${category}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IActor[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}

function getActorDetails(id: number): Promise<IActorDetails> {
  const requestOptions = {
    method: "GET",
  };
  
  return fetch(
    `${config.apiUrl}person/${id}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IActorDetails }) => {
      return responce;
    })
    .catch((err) => err);
}
function getMovieCredits(id: number): Promise<IMovie[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}person/${id}/movie_credits?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { cast: IMovie[] }) => {
      return responce.cast;
    })
    .catch((err) => err);
}
function getTVShowCredits(id: number): Promise<ITVShow[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}person/${id}/tv_credits?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { cast: ITVShow[] }) => {
      return responce.cast;
    })
    .catch((err) => err);
}
function handleResponse(response: any) {
  return response.text().then((text: string) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
