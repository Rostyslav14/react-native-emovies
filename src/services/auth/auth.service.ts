import { auth } from "../../firebase/firebase";
import {
  createUserWithEmailAndPassword,
  sendEmailVerification,
  signInWithEmailAndPassword,
  signOut,
} from "firebase/auth";

export const authService = {
  signUp,
  signIn,
  logOut,
};

async function signUp(user: { email: string; password: string }): Promise<any> {
  try {
    const userCredentials = await createUserWithEmailAndPassword(
      auth,
      user.email,
      user.password
    );
    sendEmailVerification(userCredentials.user);
    return Promise.resolve(userCredentials);
  } catch (err) {
    return Promise.reject(err);
  }
}

async function signIn(email: string, password: string): Promise<any> {
  try {
    const res = await signInWithEmailAndPassword(auth, email, password);
    return Promise.resolve(res);
  } catch (err) {
    return Promise.reject(err);
  }
}
async function logOut() {
  const res = await signOut(auth);
  return res;
}
