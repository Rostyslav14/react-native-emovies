import {IMovie} from '../helpers/types/movie';
import config from '../config';

export const collectionsService = {
  getCollectionDetails
};


function getCollectionDetails(
    id:number,
  ): Promise<any> {
    // if (id) {
      const requestOptions = {
        method: 'GET',
      };
  
      return fetch(
        `${config.apiUrl}collection/${id}?api_key=${config.apiKey}`,
        requestOptions,
      )
        .then(data => handleResponse(data))
        .then((responce: {results: IMovie[]}) => {
          return responce.results;
        })
        .catch(err => err);
    // }
  }

  function handleResponse(response: any) {
    return response.text().then((text: string) => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
        const error = (data && data.message) || response.statusText;
        return Promise.reject(error);
      }
  
      return data;
    });
  }
  
  