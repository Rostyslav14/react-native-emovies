import { IGenre } from "./../helpers/types/shared";
import config from "../config";

export const metadataService = {
  getMoviesGenres,
};

function getMoviesGenres(): Promise<IGenre[]> {
  const requestOptions = {
    method: "GET",
  };
  return fetch(
    `https://api.themoviedb.org/3/genre/movie/list?api_key=${config.apiKey}&language=en-US`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((data) => data.genres)
    .catch((err) => err);
}

function handleResponse(response: any) {
  return response.text().then((text: string) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

