import { ICastAndCrew } from "./../helpers/types/shared";
import { MOVIES_LIST_BY_ID_VARIANTS } from "./../helpers/constants/requestConstants";
import { IMovieCollection, IMovieDetails } from "./../helpers/types/movie";
import { MOVIES_LIST_REQUEST_VARIANTS } from "../helpers/constants/requestConstants";
import { IMovie } from "../helpers/types/movie";
import config from "../config";
import { IVideo } from "../helpers/types/shared";

export const movieService = {
  getList,
  getDetailsById,
  getListById,
  getVideosById,
  getCastAndCrewById,
  getCollectionById,
  rateMovie,
  deleteMovieRating
};

function getList(
  category: typeof MOVIES_LIST_REQUEST_VARIANTS[number],
  page: number = 1
): Promise<IMovie[]> {
  const requestOptions = {
    method: "GET",
  };
  return fetch(
    `${config.apiUrl}movie/${category}?api_key=${config.apiKey}&page=${page}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IMovie[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
function getDetailsById(movie_id: number): Promise<IMovieDetails> {
  const requestOptions = {
    method: "GET",
  };
  return fetch(
    `${config.apiUrl}movie/${movie_id}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IMovieDetails }) => {
      return responce;
    })
    .catch((err) => err);
}
function getListById(
  id: number,
  category: typeof MOVIES_LIST_BY_ID_VARIANTS[number],
  page: number = 1
): Promise<IMovie[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/movie/${id}/${category}?api_key=${config.apiKey}&page=${page}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IMovie[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}

function getVideosById(id: number): Promise<IVideo[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/movie/${id}/videos?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IVideo[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
function getCastAndCrewById(id: number): Promise<ICastAndCrew> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/movie/${id}/credits?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: ICastAndCrew) => {
      return responce;
    })
    .catch((err) => err);
}
function getCollectionById(id: number): Promise<IMovieCollection> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/collection/${id}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: IMovieCollection) => {
      return responce;
    })
    .catch((err) => err);
}
function rateMovie(id: number, rateValue: number): Promise<IMovieCollection> {
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json;charset=utf-8" },
    body: JSON.stringify({
      value: rateValue,
    }),
  };
  return fetch(
    `${config.apiUrl}/movie/${id}/rating?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((responce) => {
      return responce.json();
    })
    .catch((err) => console.error(err));
}
function deleteMovieRating(id: number): Promise<IMovieCollection> {
  const requestOptions = {
    method: "DELETE",
    headers: { "Content-Type": "application/json;charset=utf-8" },
  };
  return fetch(
    `${config.apiUrl}/movie/${id}/rating?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((responce) => {
      console.log("SUCCES DELETE");
      return responce.json();
    })
    .catch((err) => console.error("ERR DELETE"));
}

function handleResponse(response: any) {
  return response.text().then((text: string) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
