import config from "../config";

export const searchService = {
  getSearchList,
};

function getSearchList(query:string): Promise<any[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}search/multi?api_key=${config.apiKey}&query=${query}`,
    requestOptions
  )
    .then((data) => data.json())
    .then((responce: { results: any[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
