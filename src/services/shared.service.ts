import { SHARED_LIST_REQUEST_VARIANTS } from "../helpers/constants/requestConstants";
import { IMovie } from "../helpers/types/movie";
import config from "../config";
import { ITVShow } from "../helpers/types/tvShow";

export const sharedService = {
  getTrending,
};

function getTrending(
  category: typeof SHARED_LIST_REQUEST_VARIANTS[number],
  timestamp: "day" | "week" = "day"
): Promise<any[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}trending/${category}/${timestamp}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => data.json())
    .then((responce: { results: IMovie[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
