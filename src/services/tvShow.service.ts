import {
  ISesonDetails,
  ITVShow,
  ITVShowDetails,
} from "../helpers/types/tvShow";
import config from "../config";
import {
  TVSHOWS_LIST_BY_ID_VARIANTS,
  TVSHOWS_LIST_REQUEST_VARIANTS,
} from "../helpers/constants/requestConstants";
import { ICastAndCrew, ISeason, IVideo } from "../helpers/types/shared";

export const tvShowsService = {
  getList,
  getListById,
  getDetails,
  getVideos,
  getCastAndCrew,
  getSeasons,
  getSeasonDetails,
};

function getList(
  category: typeof TVSHOWS_LIST_REQUEST_VARIANTS[number]
): Promise<ITVShow[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}tv/${category}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: ITVShow[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
function getListById(
  id: number,
  category: typeof TVSHOWS_LIST_BY_ID_VARIANTS[number]
): Promise<ITVShow[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/tv/${id}/${category}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: ITVShow[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
function getDetails(movie_id: number): Promise<ITVShowDetails> {
  const requestOptions = {
    method: "GET",
  };
  return fetch(
    `${config.apiUrl}tv/${movie_id}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: ITVShowDetails }) => {
      return responce;
    })
    .catch((err) => err);
}
function getVideos(id: number): Promise<IVideo[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/tv/${id}/videos?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { results: IVideo[] }) => {
      return responce.results;
    })
    .catch((err) => err);
}
function getCastAndCrew(id: number): Promise<ICastAndCrew> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}/tv/${id}/credits?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: ICastAndCrew) => {
      return responce;
    })
    .catch((err) => err);
}
function getSeasons(id: number): Promise<ISeason[]> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}tv/${id}?language=en-US&api_key=${config.apiKey}&append_to_response=episode_groups`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce: { seasons: ISeason[] }) => {
      return responce.seasons;
    })
    .catch((err) => err);
}
function getSeasonDetails(
  tvId: number,
  seasonNumber: number
): Promise<ISesonDetails> {
  const requestOptions = {
    method: "GET",
  };

  return fetch(
    `${config.apiUrl}tv/${tvId}/season/${seasonNumber}?api_key=${config.apiKey}`,
    requestOptions
  )
    .then((data) => handleResponse(data))
    .then((responce:  ISesonDetails ) => {
      return responce;
    })
    .catch((err) => err);
}

function handleResponse(response: any) {
  return response.text().then((text: string) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
