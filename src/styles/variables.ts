export const styleVariables: IStyleVariables = {
  colors: {
    greenMain: "#122727",
    greenWeak: "#152222",
    greenBright: "#44a44d",
    greenDeepDark:'rgba(11,21,22,255)',

    whiteBasic:'#FFFFFF',
    whiteMain: "#ecf6f6",
    whiteWeak: "#75898b",
    whiteOpacity:'rgba(255,255,255,.2)',
  },
  fontSizes: {
    extraGiant: "24px",
    giant: "18px",
    biggest: "16px;",
    bold: "14px",
    medium: "12px",
    weak: "10px",
  },
  fontWeights: {
    biggest: 800,
    bold: 600,
    medium: 500,
    weak: 400,
  },
  paddings: {
    xAxisBasic: 24,
  },
};

interface IStyleVariables {
  colors: IColors;
  fontSizes: IFont;
  fontWeights: IFont;
  paddings: IPaddings;
}

interface IColors {
  greenMain: string;
  greenWeak: string;
  greenBright: string;
  greenDeepDark:string;

  whiteBasic:string,
  whiteMain: string;
  whiteWeak: string;
  whiteOpacity:string
}
interface IFont {
  extraGiant?: number | string;
  giant?: number | string;
  biggest?: number | string;
  bold: number | string;
  medium: number | string;
  weak: number | string;
}
interface IPaddings {
  xAxisBasic: number;
}
